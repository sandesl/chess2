package com.mygdx.game.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PositionTests {
    @Test
    public void testGetX() {
        Position position = new Position(5, 10);
        Assertions.assertEquals(5, position.getX());
    }

    @Test
    public void testGetY() {
        Position position = new Position(5, 10);
        Assertions.assertEquals(10, position.getY());
    }

    @Test
    public void testEquals() {
        Position position1 = new Position(5, 10);
        Position position2 = new Position(5, 10);
        Position position3 = new Position(10, 5);
        
        Assertions.assertTrue(position1.equals(position2));
        Assertions.assertFalse(position1.equals(position3));
    }
}