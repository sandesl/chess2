package com.mygdx.game.model.board;

import com.mygdx.game.model.Piece;
import com.mygdx.game.model.Position;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class SquareTests {

    @Test
    public void testSquareIsInitiallyNotOccupied() {
        Square square = new Square(1, 1);
        Assertions.assertFalse(square.isOccupied());
    }

    @Test
    public void testSettingPieceMakesSquareOccupied() {
        Square square = new Square(1, 1);
        Piece mockPiece = Mockito.mock(Piece.class);
        square.setPiece(mockPiece);
        Assertions.assertTrue(square.isOccupied());
    }

    @Test
    public void testRemovingPieceMakesSquareNotOccupied() {
        Square square = new Square(1, 1);
        Piece mockPiece = Mockito.mock(Piece.class);
        square.setPiece(mockPiece);
        square.removePiece();
        Assertions.assertFalse(square.isOccupied());
    }

    @Test
    public void testManuallySettingOccupiedToFalseRemovesPiece() {
        Square square = new Square(1, 1);
        Piece mockPiece = Mockito.mock(Piece.class);
        square.setPiece(mockPiece);
        square.setOccupied(false);
        Assertions.assertFalse(square.isOccupied());
        Assertions.assertNull(square.getPiece());
    }

    @Test
    public void testGetPieceReturnsCorrectPiece() {
        Square square = new Square(1, 1);
        Piece mockPiece = Mockito.mock(Piece.class);
        square.setPiece(mockPiece);
        Assertions.assertEquals(mockPiece, square.getPiece());
    }

    @Test
    public void testGetXAndGetY() {
        Square square = new Square(3, 4);
        Assertions.assertEquals(3, square.getX());
        Assertions.assertEquals(4, square.getY());
    }

    @Test
    public void testGetPositionReturnsCorrectPosition() {
        Square square = new Square(3, 4);
        Position position = square.getPosition();
        Assertions.assertEquals(3, position.getX());
        Assertions.assertEquals(4, position.getY());
    }
}
