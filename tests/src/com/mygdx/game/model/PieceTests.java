package com.mygdx.game.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.mygdx.game.model.board.Board;
import com.mygdx.game.model.board.Square;
import com.mygdx.game.model.moves.PawnMoveStrategy;
import com.mygdx.game.model.pieces.Pawn;

public class PieceTests {
    private static Board board;
    private static PawnMoveStrategy moveStrategy;

    @BeforeAll
    public static void setup() {
        board = Mockito.mock(Board.class);
        moveStrategy = Mockito.mock(PawnMoveStrategy.class);
    }

    @Test
    public void testGetColor() {
        Pawn pawn = new Pawn(Piece.ChessPieceColor.WHITE, board, moveStrategy);
        assertEquals(Piece.ChessPieceColor.WHITE, pawn.getColor());
    }

    @Test
    public void testSetSquareGetSquare() {
        Pawn pawn = new Pawn(Piece.ChessPieceColor.WHITE, board, moveStrategy);
        Square square = new Square(5, 10);
        pawn.setSquare(square);
        assertEquals(new Position(5, 10), pawn.getPosition());

        Square square2 = new Square(10, 5);
        pawn.setSquare(square2);
        assertEquals(new Position(10, 5), pawn.getPosition());
    }
}
