package com.mygdx.game.service;

import com.mygdx.game.service.APIClient.APIClientProvider;
import com.mygdx.game.service.APIClient.APIFacadeClient;

public class AndroidAPIClientProvider implements APIClientProvider {
    @Override
    public APIFacadeClient getApiClient() {
        return AndroidApiClient.getInstance();
    }
}