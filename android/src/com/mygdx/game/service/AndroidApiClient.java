package com.mygdx.game.service;

import static android.content.ContentValues.TAG;

import android.util.Log;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;
import com.mygdx.game.service.APIClient.APIFacadeClient;
import com.mygdx.game.service.data.GameSessionData;
import com.mygdx.game.service.data.GameStateData;
import com.mygdx.game.service.data.MoveData;
import com.mygdx.game.service.data.UserProfileData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class AndroidApiClient extends APIFacadeClient {
    private static AndroidApiClient instance = null;
    private final FirebaseFirestore db;

    private AndroidApiClient() {
        this.db = FirebaseFirestore.getInstance();
    }

    public static synchronized AndroidApiClient getInstance() {
        if (instance == null) {
            instance = new AndroidApiClient();
        }
        return instance;
    }

    @Override
    public void listenToGameSession(String gameSessionId, FirestoreCallback<GameSessionData> callback) {
        DocumentReference docRef = db.collection("gameSessions").document(gameSessionId);

        docRef.addSnapshotListener((snapshot, e) -> {
            if (e != null) {
                Log.w(TAG, "Listen failed.", e);
                callback.onFailure(e);
                return;
            }

            if (snapshot != null && snapshot.exists()) {
                Log.d(TAG, "Current data: " + snapshot.getData());
                GameSessionData gameSessionData = snapshot.toObject(GameSessionData.class);
                callback.onSuccess(gameSessionData);
            } else {
                Log.d(TAG, "Current data: null");
                callback.onSuccess(null);
            }
        });
    }

    @Override
    public void listenToGameState(String gameStateId, FirestoreCallback<GameStateData> callback) {
        DocumentReference docRef = db.collection("gameStates").document(gameStateId);

        docRef.addSnapshotListener((snapshot, e) -> {
            if (e != null) {
                Log.w(TAG, "Listen failed.", e);
                callback.onFailure(e);
                return;
            }

            if (snapshot != null && snapshot.exists()) {
                Log.d(TAG, "Current data: " + snapshot.getData());
                GameStateData gameStateData = snapshot.toObject(GameStateData.class);
                callback.onSuccess(gameStateData);
            } else {
                Log.d(TAG, "Current data: null");
                callback.onSuccess(null);
            }
        });
    }

    @Override
    public void createUserProfile(UserProfileData userProfileData, FirestoreCallback<String> callback) {
        db.collection("userProfiles")
                .add(userProfileData)
                .addOnSuccessListener(documentReference -> callback.onSuccess(documentReference.getId()))
                .addOnFailureListener(callback::onFailure);
    }

    @Override
    public void updateGameSession(GameSessionData gameSessionData, FirestoreCallback<String> callback) {
        db.collection("gameSessions")
                .document(gameSessionData.getGamePin())
                .set(gameSessionData)
                .addOnSuccessListener(aVoid -> callback.onSuccess(gameSessionData.getGamePin()))
                .addOnFailureListener(callback::onFailure);
    }

    @Override
    public void createGameSession(GameSessionData gameSessionData, String documentId, FirestoreCallback<String> callback) {
        db.collection("gameSessions").document(documentId)
                .set(gameSessionData)
                .addOnSuccessListener(aVoid -> callback.onSuccess(documentId))
                .addOnFailureListener(callback::onFailure);
    }

    @Override
    public void getAllGameSessionKeys(FirestoreCallback<String[]> callback) {
        db.collection("gameSessions")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        String[] keys = new String[querySnapshot.size()];
                        for (int i = 0; i < querySnapshot.size(); i++) {
                            keys[i] = querySnapshot.getDocuments().get(i).getId();
                        }
                        callback.onSuccess(keys);
                    } else {
                        Exception e = task.getException();
                        Log.w(TAG, "Error getting game session keys", e);
                        callback.onFailure(e);
                    }
                });
    }

    @Override
    public void getGameSession(String gamePin, FirestoreCallback<GameSessionData> callback) {
        db.collection("gameSessions")
                .document(gamePin)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            GameSessionData gameSession = document.toObject(GameSessionData.class);
                            Log.d(TAG, "GameSession found with gamePin: " + gamePin);
                            callback.onSuccess(gameSession);
                        } else {
                            Log.d(TAG, "No GameSession found with gamePin: " + gamePin);
                            callback.onSuccess(null);
                        }
                    } else {
                        Exception e = task.getException();
                        Log.e(TAG, "Error searching for GameSession", e);
                        callback.onFailure(e);
                    }
                });
    }

    @Override
    public void checkIfPlayerInGameSession(String displayName, FirestoreCallback<GameSessionData> callback) {
        db.collection("gameSessions")
                .whereArrayContains("players", displayName)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if (!querySnapshot.isEmpty()) {
                            DocumentSnapshot document = querySnapshot.getDocuments().get(0);
                            GameSessionData gameSession = document.toObject(GameSessionData.class);
                            Log.d(TAG, "GameSession found with player: " + displayName);
                            callback.onSuccess(gameSession);
                        } else {
                            Log.d(TAG, "No GameSession found with player: " + displayName);
                            callback.onSuccess(null);
                        }
                    } else {
                        Exception e = task.getException();
                        Log.w(TAG, "Error searching for GameSession", e);
                        callback.onFailure(e);
                    }
                });
    }

    @Override
    public void deleteGameSession(String gamePin, FirestoreCallback<String> callback) {
        db.collection("gameSessions")
                .document(gamePin)
                .delete()
                .addOnSuccessListener(aVoid -> callback.onSuccess(gamePin))
                .addOnFailureListener(callback::onFailure);
    }

    @Override
    public void createGameState(GameStateData gameState, FirestoreCallback<String> callback) {
        db.collection("gameStates")
                .add(gameState)
                .addOnSuccessListener(documentReference -> callback.onSuccess(documentReference.getId()))
                .addOnFailureListener(callback::onFailure);
    }

    @Override
    public void updateGameState(GameStateData gameStateData, FirestoreCallback<String> callback) {
        db.collection("gameStates")
                .whereEqualTo("id", gameStateData.getId())
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if (!querySnapshot.isEmpty()) {
                            DocumentSnapshot documentSnapshot = querySnapshot.getDocuments().get(0);
                            DocumentReference docRef = documentSnapshot.getReference();

                            Map<String, Object> updates = new HashMap<>();
                            updates.put("activePlayer", gameStateData.getActivePlayer());
                            updates.put("moves", gameStateData.getMoves());
                            updates.put("moveDeadline", gameStateData.getMoveDeadline());

                            docRef.update(updates)
                                    .addOnSuccessListener(aVoid -> callback.onSuccess("GameState updated with ID: " + gameStateData.getId()))
                                    .addOnFailureListener(callback::onFailure);
                        } else {
                            callback.onFailure(new Exception("No GameState found with ID: " + gameStateData.getId()));
                        }
                    } else {
                        callback.onFailure(task.getException());
                    }
                });
    }

    @Override
    public void deleteGameState(GameStateData gameStateData, FirestoreCallback<Boolean> callback) {
        db.collection("gameStates").whereEqualTo("id", gameStateData.getId()).get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        QuerySnapshot result = task.getResult();
                        if (!result.isEmpty()) {
                            DocumentSnapshot documentSnapshot = result.getDocuments().get(0);
                            db.collection("gameStates").document(documentSnapshot.getId()).delete()
                                    .addOnSuccessListener(aVoid -> callback.onSuccess(true))
                                    .addOnFailureListener(callback::onFailure);
                        } else {
                            callback.onSuccess(false);
                        }
                    } else {
                        callback.onFailure(task.getException());
                    }
                });
    }

    @Override
    public void getGameStateID(FirestoreCallback<Integer> callback) {
        db.collection("gameStates")
                .orderBy("id", com.google.firebase.firestore.Query.Direction.DESCENDING)
                .limit(1)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        QuerySnapshot result = task.getResult();
                        if (!result.isEmpty()) {
                            DocumentSnapshot documentSnapshot = result.getDocuments().get(0);
                            GameStateData lastGameState = documentSnapshot.toObject(GameStateData.class);
                            int nextId = lastGameState != null ? lastGameState.getId() + 1 : 1;
                            callback.onSuccess(nextId);
                        } else {
                            callback.onSuccess(1);
                        }
                    } else {
                        callback.onFailure(task.getException());
                    }
                });
    }

    @Override
    public void createMove(MoveData moveData, FirestoreCallback<String> callback) {
        db.collection("moves")
                .add(moveData)
                .addOnSuccessListener(documentReference -> callback.onSuccess(documentReference.getId()))
                .addOnFailureListener(callback::onFailure);
    }

    @Override
    public void getUserProfile(String displayName, FirestoreCallback<UserProfileData> callback) {
        db.collection("userProfiles")
                .whereEqualTo("displayName", displayName)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if (!querySnapshot.isEmpty()) {
                            DocumentSnapshot document = querySnapshot.getDocuments().get(0);
                            UserProfileData userProfile = document.toObject(UserProfileData.class);
                            Log.d(TAG, "UserProfile found with DisplayName: " + displayName);
                            callback.onSuccess(userProfile);
                        } else {
                            Log.d(TAG, "No UserProfile found with DisplayName: " + displayName);
                            callback.onSuccess(null);
                        }
                    } else {
                        Exception e = task.getException();
                        Log.w(TAG, "Error after search for UserProfile", e);
                        callback.onFailure(e);
                    }
                });
    }

    @Override
    public void updateUserProfile(UserProfileData userProfileData, FirestoreCallback<String> callback) {
        String displayName = userProfileData.getDisplayName();

        db.collection("userProfiles")
                .whereEqualTo("displayName", displayName)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful() && !task.getResult().isEmpty()) {
                        DocumentSnapshot document = task.getResult().getDocuments().get(0);
                        String documentId = document.getId();

                        Map<String, Object> userProfileMap = new HashMap<>();
                        userProfileMap.put("displayName", userProfileData.getDisplayName());
                        userProfileMap.put("elo", userProfileData.getElo());

                        db.collection("userProfiles").document(documentId)
                                .set(userProfileMap, SetOptions.merge())
                                .addOnSuccessListener(aVoid -> {
                                    Log.d(TAG, "UserProfile successfully updated for DisplayName: " + displayName);
                                    callback.onSuccess(documentId);
                                })
                                .addOnFailureListener(e -> {
                                    Log.w(TAG, "Error updating UserProfile for DisplayName: " + displayName, e);
                                    callback.onFailure(e);
                                });

                    } else {
                        if (task.getException() != null) {
                            Log.w(TAG, "Error finding UserProfile with DisplayName: " + displayName, task.getException());
                            callback.onFailure(task.getException());
                        } else {
                            Log.d(TAG, "No UserProfile found with DisplayName: " + displayName);
                            callback.onFailure(new Exception("No UserProfile found with DisplayName: " + displayName));
                        }
                    }
                });
    }


    @Override
    public void getGameState(String id, FirestoreCallback<GameStateData> callback) {
        db.collection("gameStates")
                .whereEqualTo("id", Integer.parseInt(id))
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if (!querySnapshot.getDocuments().isEmpty()) {
                            DocumentSnapshot document = querySnapshot.getDocuments().get(0);
                            GameStateData gameStateData = document.toObject(GameStateData.class);
                            Log.d(TAG, "GameState found with ID: " + id);
                            callback.onSuccess(gameStateData);
                        } else {
                            Log.d(TAG, "No GameState found with ID: " + id);
                            callback.onSuccess(null);
                        }
                    } else {
                        Log.w(TAG, "Error after search for GameState", task.getException());
                        callback.onFailure(task.getException());
                    }
                });
    }

    @Override
    public void getMove(String id, FirestoreCallback<MoveData> callback) {
        db.collection("moves").document(id).get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                DocumentSnapshot documentSnapshot = task.getResult();
                if (documentSnapshot.exists()) {
                    MoveData moveData = documentSnapshot.toObject(MoveData.class);
                    Log.d(TAG, "Move data found with ID: " + id);
                    callback.onSuccess(moveData);
                } else {
                    Log.d(TAG, "No move data found with ID: " + id);
                    callback.onSuccess(null);
                }
            } else {
                Exception e = task.getException();
                Log.w(TAG, "Error getting move data", e);
                callback.onFailure(e);
            }
        });
    }

    @Override
    public void getMovesID(FirestoreCallback<Integer> callback) {
        db.collection("moves")
                .orderBy("id", com.google.firebase.firestore.Query.Direction.DESCENDING)
                .limit(1)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        QuerySnapshot result = task.getResult();
                        if (!result.isEmpty()) {
                            DocumentSnapshot documentSnapshot = result.getDocuments().get(0);
                            GameStateData lastGameState = documentSnapshot.toObject(GameStateData.class);
                            int nextId = lastGameState != null ? lastGameState.getId() + 1 : 1;
                            callback.onSuccess(nextId);
                        } else {
                            callback.onSuccess(1);
                        }
                    } else {
                        callback.onFailure(task.getException());
                    }
                });
    }
    @Override
    public void getAllPlayers(FirestoreCallback<List<UserProfileData>> callback) {
        db.collection("userProfiles")
                .orderBy("elo", com.google.firebase.firestore.Query.Direction.DESCENDING)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        List<UserProfileData> userProfileList = new ArrayList<>();
                        QuerySnapshot querySnapshot = task.getResult();
                        for (DocumentSnapshot document : querySnapshot.getDocuments()) {
                            UserProfileData userProfile = document.toObject(UserProfileData.class);
                            userProfileList.add(userProfile);
                        }
                        callback.onSuccess(userProfileList);
                    } else {
                        Exception e = task.getException();
                        Log.w(TAG, "Error getting all players", e);
                        callback.onFailure(e);
                    }
                });
    }
}
