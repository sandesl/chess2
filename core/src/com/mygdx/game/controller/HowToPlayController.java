package com.mygdx.game.controller;

import com.mygdx.game.GameManager;

public class HowToPlayController extends ControllerState {
    private static int currentPage = 1;
    private static int maxPages;
    

    public HowToPlayController(ControllerStateManager controllerStateManager) {
        super(controllerStateManager);
    }

    public static void returnToMenu() {
        GameManager.getInstance().setStateManagers(GameManager.GameState.MAIN_MENU);
    }

    public static int getCurrentPage() {
        return currentPage;
    }

    public static void nextPage() {
        System.err.println("testing");
        System.err.println(currentPage);
        if (currentPage < maxPages) {
            currentPage++;
        }
    }

    public static void setMaxPages(int maxPages) {
        HowToPlayController.maxPages = maxPages;
    }

    public static void previousPage() {
        if (currentPage > 1) {
            currentPage--;
        }
    }

    public static int getMaxPages() {
        return maxPages;
    }

    public static void reset() {
        currentPage = 1;
    }
}
