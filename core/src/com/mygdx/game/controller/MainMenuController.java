package com.mygdx.game.controller;

import com.mygdx.game.GameManager;

public class MainMenuController extends ControllerState {

    public MainMenuController(ControllerStateManager controllerStateManager) {
        super(controllerStateManager);
    }

    public static void mainMenu() {
        GameManager.getInstance().setStateManagers(GameManager.GameState.MAIN_MENU);
    }

    public static void startGameLobby() {
        GameManager.getInstance().setStateManagers(GameManager.GameState.GAME_LOBBY);
    }

    public static void menuSettings() {
        GameManager.getInstance().setStateManagers(GameManager.GameState.SETTINGS);
    }

    public static void menuLeaderboard() {
        GameManager.getInstance().setStateManagers(GameManager.GameState.LEADERBOARD);
    }

    public static void menuHowToPlay() {
        GameManager.getInstance().setStateManagers(GameManager.GameState.HOW_TO_PLAY);
    }
}
