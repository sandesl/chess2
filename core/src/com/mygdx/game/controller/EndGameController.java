package com.mygdx.game.controller;

import com.mygdx.game.GameManager;
import com.mygdx.game.model.Game;
import com.mygdx.game.model.player.EloCalculator;
import com.mygdx.game.model.player.Player;
import com.mygdx.game.service.EntityService;
import com.mygdx.game.service.FirestoreCallback;
import com.mygdx.game.service.data.UserProfileData;
import java.util.ArrayList;

public class EndGameController extends ControllerState{
    private static EntityService entityService;
    public EndGameController(ControllerStateManager controllerStateManager) {
        super(controllerStateManager);
        entityService = GameManager.getInstance().getEntityService();
        concludeGame();
    }

    public void concludeGame() {
        Game game = Game.getInstance();
        Player winner = game.getWinner();
        Player loser = game.getLoser();

        Long winnerInitialElo = winner.getElo();
        Long loserInitialElo = loser.getElo();

        ArrayList<Integer> eloList = EloCalculator.Calculate2Player(winnerInitialElo, loserInitialElo, EloCalculator.GameEndState.WIN);
        updateUserProfile(winner.getName(), eloList.get(0));
        updateUserProfile(loser.getName(), eloList.get(1));

    }

    private void updateUserProfile(String playerName, Integer elo) {
        entityService.getUserProfile(playerName, new FirestoreCallback<UserProfileData>() {
            @Override
            public void onSuccess(UserProfileData userProfile) {
                userProfile.setElo(elo.longValue());
                GameManager.getInstance().findPlayerByName(userProfile.getDisplayName()).setElo(elo.longValue());
                entityService.updateUserProfile(userProfile, new FirestoreCallback<String>() {
                    @Override
                    public void onSuccess(String result) {
                        System.out.println("UserProfile updated successfully for " + playerName);
                    }

                    @Override
                    public void onFailure(Exception e) {
                        System.out.println("Failed to update UserProfile for " + playerName);
                    }
                });
            }

            @Override
            public void onFailure(Exception e) {
                System.out.println("Failed to retrieve UserProfile for " + playerName);
            }
        });
    }

    public static Player getLoser() {
        return Game.getInstance().getLoser();
    }

    public static Player getWinner() {
        return Game.getInstance().getWinner();
    }

    public static void returnToMainMenu() {
        System.out.println("Returning to main menu");
        Game.getInstance().endGame();
        GameManager.getInstance().setStateManagers(GameManager.GameState.MAIN_MENU);
    }
}
