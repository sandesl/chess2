package com.mygdx.game.controller;

import com.mygdx.game.GameManager;
import com.mygdx.game.GameManager.GameState;

import com.mygdx.game.model.Game;
import com.mygdx.game.model.GameStatus;
import com.mygdx.game.model.Piece;
import com.mygdx.game.model.Position;
import com.mygdx.game.model.board.Board;
import com.mygdx.game.model.board.Move;
import com.mygdx.game.model.board.Square;
import com.mygdx.game.model.player.Player;

import java.util.List;

public class InGameController extends ControllerState {


    public InGameController(ControllerStateManager controllerStateManager) {
        super(controllerStateManager);
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        System.out.println("InGameController touchDown");
        GameManager.getInstance().setStateManagers(GameState.MAIN_MENU);
        return false;
    }

    public static GameStatus getGameStatus() {
        return Game.getInstance().getGameStatus();
    }

    public static void setEndGameState() {
        GameManager.getInstance().setStateManagers(GameManager.GameState.END_GAME);
    }

    public static Boolean isColorsTurn(Piece.ChessPieceColor color) {
        return Game.getInstance().isColorsTurn(color);
    }

    public static Board getBoard() {
        return Game.getInstance().getBoard();
    }

    public static void MovePiece(Piece movedPiece, Square target) {
        Game.getInstance().movePiece(movedPiece, target);
    }

    public static void forfeitGame() {
        Game.getInstance().switchPlayer();
        Game.getInstance().setCheckmate();
    }

    public static void SetSquareHighlight(Position position, Boolean isHighlighted) {
        Game.getInstance().getBoard().getSquare(position.getX(), position.getY()).setIsHighlighted(isHighlighted);
    }
}
