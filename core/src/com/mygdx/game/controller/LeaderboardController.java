package com.mygdx.game.controller;

import com.badlogic.gdx.Gdx;
import com.mygdx.game.GameManager;
import com.mygdx.game.service.EntityService;
import com.mygdx.game.service.FirestoreCallback;
import com.mygdx.game.service.data.UserProfileData;
import com.mygdx.game.view.LeaderboardView;

import java.util.List;

public class LeaderboardController extends ControllerState {

    private static EntityService entityService;

    public LeaderboardController(ControllerStateManager controllerStateManager) {
        super(controllerStateManager);
        entityService = GameManager.getInstance().getEntityService();
        initLeaderboard();
    }

    private void initLeaderboard() {
        getPlayersForLeaderboard(new FirestoreCallback<List<UserProfileData>>() {
            @Override
            public void onSuccess(List<UserProfileData> result) {
                LeaderboardView.updateLeaderboard(result);
            }

            @Override
            public void onFailure(Exception e) {
                Gdx.app.error("LeaderboardController", "Failed to fetch players: " + e.getMessage()); // Log error on failure
            }
        });
    }

    private void getPlayersForLeaderboard(FirestoreCallback<List<UserProfileData>> callback) {
        entityService.getAllPlayers(new FirestoreCallback<List<UserProfileData>>() {
            @Override
            public void onSuccess(List<UserProfileData> result) {
                callback.onSuccess(result);
            }

            @Override
            public void onFailure(Exception e) {
                callback.onFailure(e);
            }
        });
    }

    public static void returnToMenu() {
        GameManager.getInstance().setStateManagers(GameManager.GameState.MAIN_MENU);
    }
}
