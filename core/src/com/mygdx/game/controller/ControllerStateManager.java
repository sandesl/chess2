package com.mygdx.game.controller;

import java.util.Stack;

public class ControllerStateManager {

    // This class is a part of the Controller module.
    // It is responsible for managing the state of the controller.
    // It is used to manage the controller states.
    // It is a stack of controller states.
    // The top of the stack is the current controller state.
    // The current controller state is the one that is used to handle input.
    // The stack is used to manage the controller states.
    

    private final Stack<ControllerState> controller;

    public ControllerStateManager() {
        controller = new Stack<ControllerState>();
    }

    public void push(ControllerState state) {
        controller.push(state);
    }

    public void pop() {
        controller.pop();
    }

    public void set(ControllerState state) {
        controller.pop();
        controller.push(state);
    }

    public ControllerState peek() {
        return controller.peek();
    }

    public int size() {
        return controller.size();
    }
}
