package com.mygdx.game.controller;

import com.badlogic.gdx.InputAdapter;
import com.mygdx.game.GameManager;

public abstract class ControllerState extends InputAdapter {

    // The controller state is an abstract class that is used to represent the state of the controller.
    // It is used to handle input from the user.
    // It is used to manage the controller state manager.
    // it is extended by other controller states.

    private ControllerStateManager controllerStateManager;

    public ControllerState(ControllerStateManager controllerStateManager) {
        this.controllerStateManager = controllerStateManager;
    }

    public ControllerStateManager getControllerStateManager() {
        return controllerStateManager;
    }

    public void setControllerStateManager(ControllerStateManager controllerStateManager) {
        this.controllerStateManager = controllerStateManager;
    }

    public static void returnToMainMenu() {
        GameManager.getInstance().setStateManagers(GameManager.GameState.MAIN_MENU);
    }
}
