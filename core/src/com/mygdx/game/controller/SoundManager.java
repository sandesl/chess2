package com.mygdx.game.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.utils.Disposable;
import java.util.HashMap;
import java.util.Map;

public class SoundManager implements Disposable {
    private static SoundManager instance;
    private static AssetManager assetManager;
    private Music currentMusic;
    private final Map<String, String> soundPaths;
    private final Map<String, Sound> soundEffects = new HashMap<>();
    private final Map<String, Music> musicTracks = new HashMap<>();
    private boolean isMusicMuted = false;
    private boolean isSoundMuted = false;
    private final Preferences prefs;
    private final float musicVolume = 0.5f;

    private SoundManager(AssetManager assetManager) {
        SoundManager.assetManager = assetManager;
        this.prefs = Gdx.app.getPreferences("Chess Evolution");
        loadPreferences();
        soundPaths = new HashMap<>();
        initializeSounds();
    }

    private void loadPreferences() {
        isMusicMuted = prefs.getBoolean("musicMuted", false);
        isSoundMuted = prefs.getBoolean("soundMuted", false);
    }

    private void initializeSounds() {
        soundPaths.put("gameMusic", "sounds/music.mp3");
        soundPaths.put("moveSound", "sounds/move.mp3");
        soundPaths.put("captureSound", "sounds/capture.mp3");
        soundPaths.put("clickSound", "sounds/click.mp3");
        loadAssets();
    }

    public void loadAssets() {
        for (Map.Entry<String, String> entry : soundPaths.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            if (key.contains("Music")) {
                assetManager.load(value, Music.class);
            } else {
                assetManager.load(value, Sound.class);
            }
        }
        assetManager.finishLoading();

        for (Map.Entry<String, String> entry : soundPaths.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            if (key.contains("Music")) {
                musicTracks.put(key, assetManager.get(value, Music.class));
            } else {
                soundEffects.put(key, assetManager.get(value, Sound.class));
            }
        }
    }

    public static SoundManager getInstance(AssetManager assetManager) {
        if (instance == null) {
            instance = new SoundManager(assetManager);
        }
        return instance;
    }

    public static SoundManager getInstance() {
        if (instance == null) {
            if (SoundManager.assetManager == null) {
                SoundManager.assetManager = new AssetManager();
            }
            instance = new SoundManager(SoundManager.assetManager);
        }
        return instance;
    }

    public void playSound(String soundKey) {
        if (!isSoundMuted && soundEffects.containsKey(soundKey)) {
            soundEffects.get(soundKey).play();
        }
    }

    public void playMusic(String musicKey) {
        if (currentMusic != null) {
            currentMusic.dispose();
        }
        if (musicTracks.containsKey(musicKey)) {
            currentMusic = musicTracks.get(musicKey);
            currentMusic.setLooping(true);
            if (!isMusicMuted) {
                currentMusic.play();
                currentMusic.setVolume(musicVolume);
            } else {
                currentMusic.setVolume(0);
            }
        }
    }

    public void toggleMusicMute() {
        isMusicMuted = !isMusicMuted;
        prefs.putBoolean("musicMuted", isMusicMuted);
        prefs.flush();

        if (currentMusic != null) {
            if (isMusicMuted) {
                currentMusic.pause();
            } else {
                currentMusic.setVolume(musicVolume);
                if (!currentMusic.isPlaying()) {
                    currentMusic.play();
                }
            }
        }
    }

    public void toggleSoundMute() {
        isSoundMuted = !isSoundMuted;
        prefs.putBoolean("soundMuted", isSoundMuted);
        prefs.flush();
    }

    public boolean isMusicMuted() {
        return isMusicMuted;
    }

    public boolean isSoundMuted() {
        return isSoundMuted;
    }

    @Override
    public void dispose() {
        for (Sound sound : soundEffects.values()) {
            sound.dispose();
        }
        for (Music music : musicTracks.values()) {
            music.dispose();
        }
        if (assetManager != null) {
            assetManager.dispose();
            assetManager = null;
        }
        instance = null;
    }
}
