package com.mygdx.game.controller;

public class SettingsController extends ControllerState {

    public SettingsController(ControllerStateManager controllerStateManager) {
        super(controllerStateManager);
    }

    public static void toggleMusic() {
        SoundManager.getInstance().toggleMusicMute();
    }

    public static void toggleSoundEffects() {
        SoundManager.getInstance().toggleSoundMute();
    }

    public static boolean isMusicMuted() {
        return SoundManager.getInstance().isMusicMuted();
    }

    public static boolean isSoundMuted() {
        return SoundManager.getInstance().isSoundMuted();
    }
}
