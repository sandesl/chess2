package com.mygdx.game.controller;

import static com.mygdx.game.GameManager.initializePlayerData;

import com.badlogic.gdx.Gdx;
import com.mygdx.game.GameManager;
import com.mygdx.game.model.Game;
import com.mygdx.game.model.player.Player;
import com.mygdx.game.service.EntityService;
import com.mygdx.game.service.FirestoreCallback;
import com.mygdx.game.service.data.GameSessionData;
import com.mygdx.game.service.data.GameStateData;
import com.mygdx.game.service.data.UserProfileData;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Json;

import java.util.ArrayList;
import java.util.List;

class Configuration {
    public ArrayList<Integer> boardSizes;
    public ArrayList<Integer> timeLimits;
}

public class GameLobbyController extends ControllerState {
    private static EntityService entityService;
    private static GameSessionData gameSessionData;
    private static ArrayList<String> loadouts;
    private static ArrayList<Integer> boardSizes;
    private static ArrayList<Integer> timeLimits;

    static {
        loadConfigurations();
        loadLoadouts();
    }

    public GameLobbyController(ControllerStateManager controllerStateManager) {
        super(controllerStateManager);
        entityService = GameManager.getInstance().getEntityService();
    }

    private static void loadConfigurations() {
        Json json = new Json();
        FileHandle configFile = Gdx.files.internal("config.json");
        boardSizes = new ArrayList<>();
        timeLimits = new ArrayList<>();
        if (configFile.exists()) {
            String configStr = configFile.readString();
            Configuration config = json.fromJson(Configuration.class, configStr);
            boardSizes.addAll(config.boardSizes);
            timeLimits.addAll(config.timeLimits);
        } else {
            Gdx.app.log("GameLobbyController", "Config file not found");
        }
    }

    private static void loadLoadouts() {
        FileHandle dirHandle = Gdx.files.internal("loadouts");
        loadouts = new ArrayList<>();
        if (dirHandle.exists() && dirHandle.isDirectory()) {
            for (FileHandle entry : dirHandle.list()) {
                String fileName = entry.name();
                if (fileName.endsWith(".json")) {
                    fileName = fileName.substring(0, fileName.length() - 5);
                }
                loadouts.add(fileName);
            }
        } else {
            Gdx.app.log("GameLobbyController", "Loadouts not found");
        }
    }

    public static ArrayList<Integer> getBoardSizes() {
        return boardSizes;
    }

    public static ArrayList<Integer> getTimeLimits() {
        return timeLimits;
    }

    public static ArrayList<String> getLoadouts() {
        return loadouts;
    }

    public static boolean startGame(ArrayList<Player> playersInLobby, int boardPreset, int timePerMove, List<String> loadouts) {
        if (playersInLobby.size() == 2) {
            GameManager.getInstance().setPlayers(playersInLobby);
            initializePlayerData();
            createGameSession(playersInLobby, boardPreset, timePerMove);
            Game.getInstance().startGame(playersInLobby, boardPreset, timePerMove, loadouts);
            GameManager.getInstance().setStateManagers(GameManager.GameState.IN_GAME);
            return true;
        } else {
            return false;
        }
    }

    public static void createGameSession(List<Player> players, int boardPreset, int timePerMove) {
        List<UserProfileData> userProfileDataList = convertPlayersToUserProfileData(players);

        gameSessionData = new GameSessionData();
        gameSessionData.setBoardPreset(boardPreset);
        gameSessionData.setTimePerMove(timePerMove);

        entityService.createGameSession(gameSessionData, userProfileDataList, new FirestoreCallback<String>() {
            @Override
            public void onSuccess(String gameSessionKey) {
                System.out.println("GameSession created with key: " + gameSessionKey);
                createGameState(userProfileDataList);
            }

            @Override
            public void onFailure(Exception e) {
                Gdx.app.log("GameLobbyController", "Failed to create GameSession", e);
            }
        });
    }

    private static void createGameState(List<UserProfileData> userProfileDataList) {
        GameStateData gameStateData = new GameStateData();
        entityService.createGameState(gameStateData, gameSessionData.getGamePin(), userProfileDataList.get(0).getDisplayName(), new FirestoreCallback<String>() {
            @Override
            public void onSuccess(String result) {
                gameStateData.setActivePlayer(userProfileDataList.get(0).getDisplayName());
                System.out.println("GameState created connected to GameSession: " + result);
            }

            @Override
            public void onFailure(Exception e) {
                Gdx.app.log("GameLobbyController", "Failed to create GameState", e);
            }
        });
    }

    private static List<UserProfileData> convertPlayersToUserProfileData(List<Player> players) {
        List<UserProfileData> userProfileDataList = new ArrayList<>();
        for (Player player : players) {
            UserProfileData userProfileData = new UserProfileData();
            userProfileData.setDisplayName(player.getName());
            userProfileDataList.add(userProfileData);
        }
        return userProfileDataList;
    }


}
