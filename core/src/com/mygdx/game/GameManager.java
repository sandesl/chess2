package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.mygdx.game.controller.ControllerState;
import com.mygdx.game.controller.ControllerStateManager;
import com.mygdx.game.controller.EndGameController;
import com.mygdx.game.controller.InGameController;
import com.mygdx.game.controller.LeaderboardController;
import com.mygdx.game.controller.MainMenuController;
import com.mygdx.game.controller.SettingsController;
import com.mygdx.game.controller.HowToPlayController;
import com.mygdx.game.model.player.Player;
import com.mygdx.game.service.FirestoreCallback;
import com.mygdx.game.service.data.UserProfileData;
import com.mygdx.game.view.EndGameView;
import com.mygdx.game.view.HowToPlayView;
import com.mygdx.game.controller.GameLobbyController;
import com.mygdx.game.service.APIClient.APIClientProvider;
import com.mygdx.game.service.EntityService;
import com.mygdx.game.view.GameLobbyView;
import com.mygdx.game.view.InGameView;
import com.mygdx.game.view.LeaderboardView;
import com.mygdx.game.view.MainMenuView;
import com.mygdx.game.view.SettingsView;
import com.mygdx.game.view.ViewState;
import com.mygdx.game.view.ViewStateManager;

import java.util.ArrayList;
import java.util.List;

public class GameManager {
    private static APIClientProvider apiClientProvider;
    private EntityService service;
    private List<Player> players = new ArrayList<>();


    public void dispose() {
        viewStateManager.dispose();
    }

    public void setApiClientProvider(APIClientProvider apiClientProvider) {
        GameManager.apiClientProvider = apiClientProvider;
        service = new EntityService(GameManager.apiClientProvider.getApiClient());
    }

    public EntityService getEntityService() {
        return service;
    }


    // Game Manager is a singleton class that manages the game state
    // It contains the view state manager and the controller state manager
    // It also contains the game state enum
    // It is used to switch between game states and to render and update the game

    public enum GameState {
        MAIN_MENU, IN_GAME, SETTINGS, GAME_LOBBY, HOW_TO_PLAY, LEADERBOARD, END_GAME
    }

    private final ViewStateManager viewStateManager = new ViewStateManager();
    private final ControllerStateManager controllerStateManager = new ControllerStateManager();

    private static GameManager instance = null;

    private GameManager(GameState gameState) {
        setStateManagers(gameState);
    }

    public static GameManager getInstance() {
        if (instance == null) {
            instance = new GameManager(GameState.MAIN_MENU);
        }
        return instance;
    }

    public void setStateManagers(GameState gameState) {
        switch (gameState) {
            case MAIN_MENU:
                setState(new MainMenuView(viewStateManager), new MainMenuController(controllerStateManager));
                break;
            case GAME_LOBBY:
                setState(new GameLobbyView(viewStateManager), new GameLobbyController(controllerStateManager));
                break;
            case IN_GAME:
                setState(new InGameView(viewStateManager), new InGameController(controllerStateManager));
                break;
            case SETTINGS:
                setState(new SettingsView(viewStateManager), new SettingsController(controllerStateManager));
                break;
            case HOW_TO_PLAY:
                setState(new HowToPlayView(viewStateManager), new HowToPlayController(controllerStateManager));
                break;
            case LEADERBOARD:
                setState(new LeaderboardView(viewStateManager), new LeaderboardController(controllerStateManager));
                break;
            case END_GAME:
                setState(new EndGameView(viewStateManager), new EndGameController(controllerStateManager));
                break;
            default:
                Gdx.app.error("GameManager", "Invalid game state");
                break;

        }
    }

    private void setState(ViewState viewState, ControllerState controllerState) {
        if (viewStateManager.size() > 0) {
            viewStateManager.pop();
        }
        if (controllerStateManager.size() > 0) {
            controllerStateManager.pop();
        }
        viewStateManager.push(viewState);
        controllerStateManager.push(controllerState);
    }

    // used as the inverse of the above
    public void popStates() {
        viewStateManager.pop();
        controllerStateManager.pop();
    }

    public void render() {
        viewStateManager.render();
    }

    public ViewStateManager getViewStateManager() {
        return viewStateManager;
    }

    public ControllerStateManager getControllerStateManager() {
        return controllerStateManager;
    }

    public void resize(int width, int height) {
        viewStateManager.resize(width, height);
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public Player findPlayerByName(String name) {
        for (Player player : players) {
            if (player.getName().equals(name)) {
                return player;
            }
        }
        return null;
    }

    public static void initializePlayerData() {
        EntityService entityService = EntityService.getInstance(apiClientProvider.getApiClient());
        for (Player player : getInstance().getPlayers()) {
            entityService.getUserProfile(player.getName(), new FirestoreCallback<UserProfileData>() {
                @Override
                public void onSuccess(UserProfileData userProfile) {
                    if (userProfile != null && userProfile.getElo() != null) {
                        Gdx.app.log("GameManager", "Found user: " + player.getName() + ", with elo: " + player.getElo());
                        player.setElo(userProfile.getElo());
                    } else {
                        Gdx.app.log("GameManager", "Found user: " + player.getName() + ", with elo: " + player.getElo());
                        player.setElo(1200L);
                    }
                }

                @Override
                public void onFailure(Exception e) {
                    Gdx.app.error("GameManager", "Failed to fetch user profile for " + player.getName(), e);
                    player.setElo(1200L);
                }
            });
        }
    }

}
