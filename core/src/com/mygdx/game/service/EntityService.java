package com.mygdx.game.service;

import com.mygdx.game.service.APIClient.APIFacadeClient;
import com.mygdx.game.service.data.GameSessionData;
import com.mygdx.game.service.data.MoveData;
import com.mygdx.game.service.data.GameStateData;
import com.mygdx.game.service.data.UserProfileData;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class EntityService {
    private static volatile EntityService instance;
    private final APIFacadeClient apiClient;

    public EntityService(APIFacadeClient apiClient) {
        this.apiClient = apiClient;
    }

    public static EntityService getInstance(APIFacadeClient apiClient) {
        if (instance == null) {
            synchronized (EntityService.class) {
                if (instance == null) {
                    instance = new EntityService(apiClient);
                }
            }
        }
        return instance;
    }

    public void listenToGameSession(String gameSessionId, FirestoreCallback<GameSessionData> callback) {
        apiClient.listenToGameSession(gameSessionId, callback);
    }

    public void listenToGameState(String gameStateId, FirestoreCallback<GameStateData> callback) {
        apiClient.listenToGameState(gameStateId, callback);
    }

    public void createUserProfile(UserProfileData userProfileData, FirestoreCallback<String> callback) {
        apiClient.getUserProfile(userProfileData.getDisplayName(), new FirestoreCallback<UserProfileData>() {
            @Override
            public void onSuccess(UserProfileData existingUserProfile) {
                if (existingUserProfile == null) {
                    apiClient.createUserProfile(userProfileData, callback);
                } else {
                    callback.onFailure(new Exception("UserProfile already exists"));
                }
            }
            @Override
            public void onFailure(Exception e) {
                callback.onFailure(e);
            }
        });
    }

    public void createGameState(GameStateData gameStateData, String gameSessionKey, String displayName, FirestoreCallback<String> callback) {
        apiClient.getUserProfile(displayName, new FirestoreCallback<UserProfileData>() {
            @Override
            public void onSuccess(UserProfileData userProfile) {
                if (userProfile != null) {
                    gameStateData.setActivePlayer(userProfile.getDisplayName());
                    getAndSetGameStateID(gameStateData, gameSessionKey, callback);
                } else {
                    callback.onFailure(new Exception("No UserProfile found with displayName: " + displayName));
                }
            }

            @Override
            public void onFailure(Exception e) {
                callback.onFailure(e);
            }
        });
    }


    private void getAndSetGameStateID(GameStateData gameStateData, String gameSessionKey, FirestoreCallback<String> callback) {
        apiClient.getGameStateID(new FirestoreCallback<Integer>() {
            @Override
            public void onSuccess(Integer id) {
                gameStateData.setId(id);
                finalizeGameStateCreation(gameStateData, gameSessionKey, callback);
            }

            @Override
            public void onFailure(Exception e) {
                callback.onFailure(e);
            }
        });
    }

    public void createGameSession(GameSessionData gameSessionData, List<UserProfileData> userProfileDataList, FirestoreCallback<String> callback) {
        ensureUserProfiles(userProfileDataList, 0, new FirestoreCallback<List<UserProfileData>>() {
            @Override
            public void onSuccess(List<UserProfileData> userProfileDatas) {
                generateUniqueGameKey(new FirestoreCallback<String>() {
                    @Override
                    public void onSuccess(String gameKey) {
                        gameSessionData.setGamePin(gameKey);

                        List<String> playerIds = userProfileDatas.stream()
                                .map(UserProfileData::getDisplayName)
                                .collect(Collectors.toList());

                        gameSessionData.setPlayers(playerIds);
                        apiClient.createGameSession(gameSessionData, gameKey, callback);
                    }

                    @Override
                    public void onFailure(Exception e) {
                        callback.onFailure(e);
                    }
                });
            }

            @Override
            public void onFailure(Exception e) {
                callback.onFailure(e);
            }
        });
    }

    private void ensureUserProfiles(List<UserProfileData> userProfileDataList, int index, FirestoreCallback<List<UserProfileData>> callback) {
        if (index >= userProfileDataList.size()) {
            callback.onSuccess(userProfileDataList); // All profiles processed
            return;
        }
        ensureUserProfile(userProfileDataList.get(index), new FirestoreCallback<UserProfileData>() {
            @Override
            public void onSuccess(UserProfileData userProfileData) {
                // Profile ensured, proceed with the next one
                ensureUserProfiles(userProfileDataList, index + 1, callback);
            }

            @Override
            public void onFailure(Exception e) {
                callback.onFailure(e); // Stop processing and report failure
            }
        });
    }



    private void finalizeGameStateCreation(GameStateData gameStateData, String gameSessionKey, FirestoreCallback<String> callback) {
        apiClient.createGameState(gameStateData, new FirestoreCallback<String>() {
            @Override
            public void onSuccess(String result) {
                updateGameSessionWithGameSessionKey(gameSessionKey, Integer.toString(gameStateData.getId()), callback);
                callback.onSuccess("GameState created with documentID: " + result);
            }

            @Override
            public void onFailure(Exception e) {
                callback.onFailure(e);
            }
        });
    }

    private void updateGameSessionWithGameSessionKey(String gameSessionKey, String gameStateId, FirestoreCallback<String> callback){
        apiClient.getGameSession(gameSessionKey, new FirestoreCallback<GameSessionData>() {
            @Override
            public void onSuccess(GameSessionData result) {
                if (result != null) {
                    result.setGameStateId(Integer.parseInt(gameStateId));
                    updateGameSession(result, callback);
                } else {
                    callback.onFailure(new Exception("GameSession not found with key: " + gameSessionKey));
                }
            }

            @Override
            public void onFailure(Exception e) {
                callback.onFailure(e);
            }
        });
    }
    private void generateUniqueGameKey(FirestoreCallback<String> callback) {
        String gameKey = generateRandomString(4);
        apiClient.getGameSession(gameKey, new FirestoreCallback<GameSessionData>() {
            @Override
            public void onSuccess(GameSessionData gameSessionData) {
                if (gameSessionData == null) {
                    callback.onSuccess(gameKey);
                } else {
                    generateUniqueGameKey(callback);
                }
            }

            @Override
            public void onFailure(Exception e) {
                callback.onFailure(e);
            }
        });
    }

    public void getGameState(String id, FirestoreCallback<GameStateData> callback) {
        apiClient.getGameState(id, new FirestoreCallback<GameStateData>() {
            @Override
            public void onSuccess(GameStateData result) {
                callback.onSuccess(result);
            }

            @Override
            public void onFailure(Exception e) {
                callback.onFailure(e);
            }
        });
    }
    private String generateRandomString(int length) {
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        Random random = new Random();
        StringBuilder gameKey = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            gameKey.append(characters.charAt(random.nextInt(characters.length())));
        }
        return gameKey.toString();
    }

    public void updateGameSession(GameSessionData gameSessionData, FirestoreCallback<String> callback) {
        apiClient.updateGameSession(gameSessionData, callback);
    }

    public void deleteGameSession(String gameSessionKey, FirestoreCallback<String> callback) {
        apiClient.deleteGameSession(gameSessionKey, callback);
    }

    public void getGameSession(String gameSessionKey, FirestoreCallback<GameSessionData> callback) {
        apiClient.getGameSession(gameSessionKey, callback);
    }

    public void joinGameSession(String gameSessionKey, String displayName, FirestoreCallback<String> callback) {
        apiClient.checkIfPlayerInGameSession(displayName, new FirestoreCallback<GameSessionData>() {
            @Override
            public void onSuccess(GameSessionData existingGameSession) {
                if (existingGameSession != null) {
                    callback.onFailure(new Exception("Player is already in a GameSession with key: " + existingGameSession.getGamePin()));
                } else {
                    proceedToJoinGameSession(gameSessionKey, displayName, callback);
                }
            }

            @Override
            public void onFailure(Exception e) {
                callback.onFailure(e);
            }
        });
    }

    public void deleteGameState(GameStateData gameStateData, FirestoreCallback<Boolean> callback) {
        getGameState(Integer.toString(gameStateData.getId()), new FirestoreCallback<GameStateData>() {
            @Override
            public void onSuccess(GameStateData result) {
                apiClient.deleteGameState(gameStateData, callback);
            }

            @Override
            public void onFailure(Exception e) {
                callback.onFailure(e);
            }
        });
    }

    public void updateGameState(GameStateData gameStateData, FirestoreCallback<String> callback){
        getGameState(Integer.toString(gameStateData.getId()), new FirestoreCallback<GameStateData>() {
            @Override
            public void onSuccess(GameStateData result) {
                result.setActivePlayer(gameStateData.getActivePlayer());
                result.setMoves(gameStateData.getMoves());
                result.setMoveDeadline(gameStateData.getMoveDeadline());
                apiClient.updateGameState(result, callback);
            }

            @Override
            public void onFailure(Exception e) {
                callback.onFailure(e);
            }
        });
    }
    private void proceedToJoinGameSession(String gameSessionKey, String displayName, FirestoreCallback<String> callback) {
        apiClient.getGameSession(gameSessionKey, new FirestoreCallback<GameSessionData>() {
            @Override
            public void onSuccess(GameSessionData gameSessionData) {
                if (gameSessionData == null) {
                    callback.onFailure(new Exception("GameSession does not exist"));
                } else if (gameSessionData.getPlayers().contains(displayName)) {
                    callback.onFailure(new Exception("Player already in this GameSession"));
                } else if (gameSessionData.getPlayers().size() >= gameSessionData.getMaxPlayers()) {
                    callback.onFailure(new Exception("GameSession is full"));
                } else {
                    gameSessionData.getPlayers().add(displayName);
                    apiClient.updateGameSession(gameSessionData, new FirestoreCallback<String>() {
                        @Override
                        public void onSuccess(String result) {
                            callback.onSuccess(gameSessionKey);
                        }

                        @Override
                        public void onFailure(Exception e) {
                            callback.onFailure(e);
                        }
                    });
                }
            }

            @Override
            public void onFailure(Exception e) {
                callback.onFailure(e);
            }
        });
    }

    public void createMove(MoveData moveData, String gameStateId, FirestoreCallback<String> callback) {
        getGameState(gameStateId, new FirestoreCallback<GameStateData>() {
            @Override
            public void onSuccess(GameStateData result) {
                setMovesID(moveData, result, callback);
            }

            @Override
            public void onFailure(Exception e) {
                callback.onFailure(e);
            }
        });
    }

    private void setMovesID(MoveData moveData, GameStateData gameStateData, FirestoreCallback<String> callback){
        apiClient.getMovesID(new FirestoreCallback<Integer>() {
            @Override
            public void onSuccess(Integer id) {
                moveData.setId(id);
                createMovesAndAddToGameState(moveData, gameStateData, callback);
            }

            @Override
            public void onFailure(Exception e) {
                callback.onFailure(e);
            }
        });
    }

    private void createMovesAndAddToGameState(MoveData moveData, GameStateData gameStateData, FirestoreCallback<String> callback){
        apiClient.createMove(moveData, new FirestoreCallback<String>() {
            @Override
            public void onSuccess(String result) {
                System.out.println("Move created with document ID: " + result);
                updateMovesInGameState(gameStateData, moveData, callback);
            }

            @Override
            public void onFailure(Exception e) {
                callback.onFailure(e);
            }
        });
    }

    public void getMove(MoveData moveData, FirestoreCallback<MoveData> callback) {
        apiClient.getMove(Integer.toString(moveData.getId()), callback);
    }

    private void updateMovesInGameState(GameStateData gameStateData, MoveData moveData, FirestoreCallback<String> callback){
        List<MoveData> moves;
        if (gameStateData.getMoves() == null) {
            moves = new ArrayList<MoveData>();
        } else {
            moves = gameStateData.getMoves();
        }
        moves.add(moveData);
        gameStateData.setMoves(moves);
        apiClient.updateGameState(gameStateData, new FirestoreCallback<String>() {
            @Override
            public void onSuccess(String result) {
                System.out.println("Added the move: [" + moveData.getFrom().getX() + ", " + moveData.getFrom().getY() + "], [" + moveData.getTo().getX() + ", " + moveData.getTo().getY() + "] to the game state with ID: " + gameStateData.getId());
                callback.onSuccess(result);
            }

            @Override
            public void onFailure(Exception e) {
                callback.onFailure(e);
            }
        });
    }

    public void ensureUserProfile(UserProfileData userProfileData, FirestoreCallback<UserProfileData> callback) {
        apiClient.getUserProfile(userProfileData.getDisplayName(), new FirestoreCallback<UserProfileData>() {
            @Override
            public void onSuccess(UserProfileData existingUserProfile) {
                if (existingUserProfile == null) {
                    createUserAndNotify(userProfileData, callback);
                } else {
                    callback.onSuccess(existingUserProfile);
                }
            }

            @Override
            public void onFailure(Exception e) {
                callback.onFailure(e);
            }
        });
    }

    private void createUserAndNotify(UserProfileData userProfileData, FirestoreCallback<UserProfileData> callback) {
        apiClient.createUserProfile(userProfileData, new FirestoreCallback<String>() {
            @Override
            public void onSuccess(String userId) {
                // Assuming the creation returns the ID, and you can fetch the profile directly afterwards
                notifyUserProfileCreated(userProfileData.getDisplayName(), callback);
            }

            @Override
            public void onFailure(Exception e) {
                callback.onFailure(e);
            }
        });
    }

    private void notifyUserProfileCreated(String displayName, FirestoreCallback<UserProfileData> callback) {
        apiClient.getUserProfile(displayName, new FirestoreCallback<UserProfileData>() {
            @Override
            public void onSuccess(UserProfileData userProfile) {
                callback.onSuccess(userProfile);
            }
            @Override
            public void onFailure(Exception e) {
                callback.onFailure(e);
            }
        });
    }

    public void getUserProfile(String playerName, FirestoreCallback<UserProfileData> callback) {
        apiClient.getUserProfile(playerName, new FirestoreCallback<UserProfileData>() {
            @Override
            public void onSuccess(UserProfileData result) {
                callback.onSuccess(result);
            }

            @Override
            public void onFailure(Exception e) {
                callback.onFailure(e);
            }
        });
    }

    public void updateUserProfile(UserProfileData userProfileData, FirestoreCallback<String> callback) {
        apiClient.updateUserProfile(userProfileData, new FirestoreCallback<String>() {
            @Override
            public void onSuccess(String result) {
                callback.onSuccess(result);
            }

            @Override
            public void onFailure(Exception e) {
                callback.onFailure(e);
            }
        });
    }

    public void getAllPlayers(FirestoreCallback<List<UserProfileData>> onSuccessCallback) {
        apiClient.getAllPlayers(new FirestoreCallback<List<UserProfileData>>() {
            @Override
            public void onSuccess(List<UserProfileData> result) {
                onSuccessCallback.onSuccess(result);
            }

            @Override
            public void onFailure(Exception e) {
                onSuccessCallback.onFailure(e);
            }
        });
    }


}
