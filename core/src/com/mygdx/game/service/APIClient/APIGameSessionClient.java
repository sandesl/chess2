package com.mygdx.game.service.APIClient;

import com.mygdx.game.service.FirestoreCallback;
import com.mygdx.game.service.data.GameSessionData;

public interface APIGameSessionClient {
    void createGameSession(GameSessionData gameSessionData, String documentId, FirestoreCallback<String> callback);
    void updateGameSession(GameSessionData gameSessionData, FirestoreCallback<String> callback);
    void checkIfPlayerInGameSession(String displayName, FirestoreCallback<GameSessionData> callback);
    void getAllGameSessionKeys(FirestoreCallback<String[]> callback);
    void deleteGameSession(String gamePin, FirestoreCallback<String> callback);
    void getGameSession(String gamePin, FirestoreCallback<GameSessionData> callback);

}
