package com.mygdx.game.service.APIClient;

public interface APIClientProvider {
    APIFacadeClient getApiClient();
}