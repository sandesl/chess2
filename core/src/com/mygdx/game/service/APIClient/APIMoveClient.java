package com.mygdx.game.service.APIClient;

import com.mygdx.game.service.FirestoreCallback;
import com.mygdx.game.service.data.MoveData;

public interface APIMoveClient {
    void createMove(MoveData moveData, FirestoreCallback<String> callback);
    void getMove(String id, FirestoreCallback<MoveData> callback);
    void getMovesID(FirestoreCallback<Integer> callback);

}
