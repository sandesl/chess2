package com.mygdx.game.service.APIClient;

import com.mygdx.game.service.FirestoreCallback;
import com.mygdx.game.service.data.GameStateData;

public interface APIGameStateClient {
    void createGameState(GameStateData gameState, FirestoreCallback<String> callback);
    void updateGameState(GameStateData gameStateData, FirestoreCallback<String> callback);
    void deleteGameState(GameStateData gameStateData, FirestoreCallback<Boolean> callback);
    void getGameStateID(FirestoreCallback<Integer> callback);
    void getGameState(String id, FirestoreCallback<GameStateData> callback);

}
