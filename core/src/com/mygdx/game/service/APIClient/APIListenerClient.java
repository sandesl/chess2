package com.mygdx.game.service.APIClient;

import com.mygdx.game.service.FirestoreCallback;
import com.mygdx.game.service.data.GameSessionData;
import com.mygdx.game.service.data.GameStateData;

public interface APIListenerClient {
    void listenToGameSession(String gameSessionId, FirestoreCallback<GameSessionData> callback);
    void listenToGameState(String gameSessionId, FirestoreCallback<GameStateData> callback);
}
