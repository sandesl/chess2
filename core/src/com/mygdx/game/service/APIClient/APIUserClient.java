package com.mygdx.game.service.APIClient;

import com.mygdx.game.service.FirestoreCallback;
import com.mygdx.game.service.data.UserProfileData;
import java.util.List;

public interface APIUserClient {
    void createUserProfile(UserProfileData userProfileData, FirestoreCallback<String> callback);
    void getUserProfile(String displayName, FirestoreCallback<UserProfileData> callback);
    void updateUserProfile(UserProfileData userProfileData, FirestoreCallback<String> callback);
    void getAllPlayers(FirestoreCallback<List<UserProfileData>> callback);
}