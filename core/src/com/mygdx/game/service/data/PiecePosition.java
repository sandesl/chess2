package com.mygdx.game.service.data;

public class PiecePosition {
    private int player;
    private String piece;

    public PiecePosition() {
    }

    public PiecePosition(int player, String piece) {
        this.player = player;
        this.piece = piece;
    }

    public int getPlayer() {
        return player;
    }

    public void setPlayer(int player) {
        this.player = player;
    }

    public String getPiece() {
        return piece;
    }

    public void setPiece(String piece) {
        this.piece = piece;
    }
}