package com.mygdx.game.service.data;

public class UserProfileData {
    private String displayName;
    private Long elo;

    public UserProfileData() {
    }

    public UserProfileData(String displayName, Long elo) {
        this.displayName = displayName;
        this.elo = elo;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Long getElo() {
        return elo;
    }

    public void setElo(Long elo) {
        this.elo = elo;
    }

    public String toString() {
        return "UserProfileData{" + "displayName=" + displayName + ", elo=" + elo + '}';
    }
}
