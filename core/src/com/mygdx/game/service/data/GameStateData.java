package com.mygdx.game.service.data;

import java.util.Date;
import java.util.List;

public class GameStateData {
    private int id;
    private String activePlayer;
    private Date moveDeadline;
    private List<MoveData> moves;

    public GameStateData() {
    }


    public GameStateData(int id, String activePlayer, Date moveDeadline, List<MoveData> moves) {
        this.id = id;
        this.activePlayer = activePlayer;
        this.moveDeadline = moveDeadline;
        this.moves = moves;
    }

    // Getters and setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getActivePlayer() {
        return activePlayer;
    }

    public void setActivePlayer(String activePlayer) {
        this.activePlayer = activePlayer;
    }

    public Date getMoveDeadline() {
        return moveDeadline;
    }

    public void setMoveDeadline(Date moveDeadline) {
        this.moveDeadline = moveDeadline;
    }

    public List<MoveData> getMoves() {
        return moves;
    }

    public void setMoves(List<MoveData> moves) {
        this.moves = moves;
    }
}
