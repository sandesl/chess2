package com.mygdx.game.service.data;

public class MoveData {
    private int id;
    private Square from;
    private Square to;

    public MoveData() {
    }

    public MoveData(int id, Square from, Square to) {
        this.id = id;
        this.from = from;
        this.to = to;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Square getFrom() {
        return from;
    }

    public void setFrom(Square from) {
        this.from = from;
    }

    public Square getTo() {
        return to;
    }

    public void setTo(Square to) {
        this.to = to;
    }
}
