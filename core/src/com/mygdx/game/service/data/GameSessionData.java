package com.mygdx.game.service.data;

import java.util.List;

public class GameSessionData {
    private String gamePin;
    private List<String> players;
    private int maxPlayers;
    private int boardPreset;
    private int timePerMove;
    private PiecePosition[][] startingLayout;
    private int gameStateId;

    public GameSessionData() {
    }

    public GameSessionData(String gamePin, List<String> players, int maxPlayers, int boardPreset, int timePerMove, PiecePosition[][] startingLayout, int gameStateId) {
        this.gamePin = gamePin;
        this.players = players;
        this.maxPlayers = maxPlayers;
        this.boardPreset = boardPreset;
        this.timePerMove = timePerMove;
        this.startingLayout = startingLayout;
        this.gameStateId = gameStateId;
    }

    public String getGamePin() {
      return gamePin;
    }

    public void setGamePin(String gamePin) {
      this.gamePin = gamePin;
    }

    public List<String> getPlayers() {
      return players;
    }

    public void setPlayers(List<String> players) {
      this.players = players;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public void setMaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;
    }

    public int getBoardPreset() {
        return boardPreset;
    }

    public void setBoardPreset(int boardPreset) {
        this.boardPreset = boardPreset;
    }

    public int getTimePerMove() {
        return timePerMove;
    }

    public void setTimePerMove(int timePerMove) {
        this.timePerMove = timePerMove;
    }

    public PiecePosition[][] getStartingLayout() {
        return startingLayout;
    }

    public void setStartingLayout(PiecePosition[][] startingLayout) {
        this.startingLayout = startingLayout;
    }

    public int getGameStateId() {
        return gameStateId;
    }

    public void setGameStateId(int gameStateId) {
        this.gameStateId = gameStateId;
    }
}
