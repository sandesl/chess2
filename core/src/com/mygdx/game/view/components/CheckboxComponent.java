package com.mygdx.game.view.components;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.mygdx.game.view.FontCollection;
import com.mygdx.game.view.ViewState;

public class CheckboxComponent {

    private final CheckBox.CheckBoxStyle checkBoxStyle;

    public CheckboxComponent() {
        TextureAtlas atlas = new TextureAtlas(Gdx.files.internal("data/uiskin.atlas"));

        int scale = 5;

        this.checkBoxStyle = new CheckBox.CheckBoxStyle();
        TextureRegionDrawable checkedDrawable = new TextureRegionDrawable(atlas.findRegion("check-on"));
        checkedDrawable.setMinWidth(checkedDrawable.getMinWidth() * scale);
        checkedDrawable.setMinHeight(checkedDrawable.getMinHeight() * scale);

        TextureRegionDrawable uncheckedDrawable = new TextureRegionDrawable(atlas.findRegion("check-off"));
        uncheckedDrawable.setMinWidth(uncheckedDrawable.getMinWidth() * scale);
        uncheckedDrawable.setMinHeight(uncheckedDrawable.getMinHeight() * scale);

        checkBoxStyle.checkboxOn = checkedDrawable;
        checkBoxStyle.checkboxOff = uncheckedDrawable;


        checkBoxStyle.font = FontCollection.getFont(ViewState.getScaledLength(85));
        checkBoxStyle.fontColor = Color.BLACK;
    }

    /**
     * Creates a new CheckBox with the defined style.
     * @param text The text to be displayed next to the checkbox.
     * @param isChecked The initial checked state of the checkbox.
     * @return A styled CheckBox instance.
     */
    public CheckBox createCheckbox(String text, boolean isChecked, InputListener listener) {
        String paddedText = " " + text;
        CheckBox checkBox = new CheckBox(paddedText, checkBoxStyle);
        checkBox.addListener(listener);
        checkBox.setChecked(isChecked);
        return checkBox;
    }

}
