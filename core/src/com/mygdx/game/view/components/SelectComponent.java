package com.mygdx.game.view.components;

import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.mygdx.game.view.FontCollection;
import com.mygdx.game.view.ViewState;

public class SelectComponent {

    private final Skin skin;

    public SelectComponent(Skin skin) {
        this.skin = skin;
    }

    /**
     * Creates a styled SelectBox with the given items.
     *
     * @param items The items to be displayed in the SelectBox.
     * @return A styled SelectBox with the given items.
     */
    public SelectBox<String> createSelectBox(String... items) {
        SelectBox<String> selectBox = new SelectBox<>(skin);
        selectBox.setItems(items);
        selectBox.getStyle().font = FontCollection.getFont(ViewState.getScaledLength(45));
        selectBox.getStyle().listStyle.font = FontCollection.getFont(ViewState.getScaledLength(45));
        return selectBox;
    }

    /**
     * Gets the selected value from the given SelectBox.
     *
     * @param selectBox The SelectBox from which to get the selected value.
     * @return The currently selected item in the given SelectBox.
     */
    public String getSelectedValue(SelectBox<String> selectBox) {
        return selectBox.getSelected();
    }
}
