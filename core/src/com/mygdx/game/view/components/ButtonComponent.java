package com.mygdx.game.view.components;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.mygdx.game.controller.SoundManager;
import com.mygdx.game.view.FontCollection;
import com.mygdx.game.view.ViewState;

public class ButtonComponent {

    private static final int WIDTH = ViewState.getScaledLength(800);
    private static final int HEIGHT = ViewState.getScaledLength(175);
    private static final int BORDER_RADIUS = ViewState.getScaledLength(25);
    private static final int FONT_SIZE = ViewState.getScaledLength(80);

    private TextButton.TextButtonStyle defaultButtonStyle;
    private TextButton.TextButtonStyle greenButtonStyle;

    public ButtonComponent() {
        initStyles();
    }

    private void initStyles() {
        defaultButtonStyle = createButtonStyle(Color.valueOf("#E5E7EB"), Color.valueOf("#B3B4B7"), Color.valueOf("#1F2A37"));
        greenButtonStyle = createButtonStyle(Color.valueOf("#36786F"), Color.valueOf("#1F453F"), Color.WHITE);
    }

    private Drawable createDrawable(Color color) {
        Pixmap pixmap = new Pixmap(WIDTH, HEIGHT, Pixmap.Format.RGBA8888);
        pixmap.setColor(color);
        pixmap.fillRectangle(0, BORDER_RADIUS, pixmap.getWidth(), pixmap.getHeight() - 2 * BORDER_RADIUS);
        pixmap.fillRectangle(BORDER_RADIUS, 0, pixmap.getWidth() - 2 * BORDER_RADIUS, pixmap.getHeight());
        pixmap.fillCircle(BORDER_RADIUS, BORDER_RADIUS, BORDER_RADIUS);
        pixmap.fillCircle(pixmap.getWidth() - BORDER_RADIUS - 1, BORDER_RADIUS, BORDER_RADIUS);
        pixmap.fillCircle(BORDER_RADIUS, pixmap.getHeight() - BORDER_RADIUS - 1, BORDER_RADIUS);
        pixmap.fillCircle(pixmap.getWidth() - BORDER_RADIUS - 1, pixmap.getHeight() - BORDER_RADIUS - 1, BORDER_RADIUS);

        Texture texture = new Texture(pixmap);
        pixmap.dispose();
        return new TextureRegionDrawable(new TextureRegion(texture));
    }

    private TextButton.TextButtonStyle createButtonStyle(Color upColor, Color downColor, Color fontColor) {
        TextButton.TextButtonStyle style = new TextButton.TextButtonStyle();
        style.up = createDrawable(upColor);
        style.down = createDrawable(downColor);
        style.font = FontCollection.getFont(FONT_SIZE);
        style.fontColor = fontColor;
        return style;
    }

    private TextButton createButton(String buttonText, TextButton.TextButtonStyle style, InputListener additionalListener) {
        TextButton button = new TextButton(buttonText, style);
        button.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                SoundManager.getInstance().playSound("clickSound");
                return additionalListener.touchDown(event, x, y, pointer, button);
            }
        });
        button.setSize(WIDTH, HEIGHT);
        return button;
    }

    public TextButton createDefaultButton(String buttonText, InputListener listener) {
        return createButton(buttonText, defaultButtonStyle, listener);
    }

    public TextButton createGreenButton(String buttonText, InputListener listener) {
        return createButton(buttonText, greenButtonStyle, listener);
    }
}
