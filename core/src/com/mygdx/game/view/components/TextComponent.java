package com.mygdx.game.view.components;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.mygdx.game.view.FontCollection;

public class TextComponent {

    private final Label.LabelStyle titleStyle;

    public TextComponent() {
        titleStyle = new Label.LabelStyle();


        titleStyle.font = FontCollection.getFont(70);
        titleStyle.fontColor = Color.valueOf("#111928");
    }

    public Label createTitle(String titleText) {
        return new Label(titleText, titleStyle);
    }
}
