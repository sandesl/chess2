package com.mygdx.game.view.components;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextArea;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.mygdx.game.view.FontCollection;
import com.mygdx.game.view.ViewState;

public class TextInputComponent {

    private final Skin skin; // LibGDX skin for styling UI components

    public TextInputComponent(Skin skin) {
        this.skin = skin;
    }

    /**
     * Creates a styled TextField with an optional ChangeListener to execute functions on input.
     *
     * @param hintText Hint text to display when the TextField is empty.
     * @param listener An optional ChangeListener that fires every time something is written in the TextField.
     * @return A styled TextField with the specified hint text and listener.
     */
    public TextField createTextField(String hintText, ChangeListener listener) {
        TextField textField = new TextArea("", skin);

        textField.getStyle().font = FontCollection.getFont(ViewState.getScaledLength(65));
        textField.setMessageText(hintText);

        if (listener != null) {
            textField.addListener(listener); // Add l istener to execute functions on input
        } 
        return textField;
    }

    /**
     * Retrieves the current text value from the given TextField.
     *
     * @param textField The TextField from which to get the text value.
     * @return The current text value of the specified TextField.
     */
    public String getTextValue(TextField textField) {
        return textField.getText();
    }
}
