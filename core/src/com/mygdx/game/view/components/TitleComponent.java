package com.mygdx.game.view.components;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.mygdx.game.view.FontCollection;
import com.mygdx.game.view.ViewState;

public class TitleComponent {

    private final Label.LabelStyle titleStyle;

    public TitleComponent() {
        titleStyle = new Label.LabelStyle();

        // Set the font and color for the title
        titleStyle.font = FontCollection.getFont(ViewState.getScaledLength(125)); // Assuming you have a method to get
                                                                                  // fonts
        titleStyle.fontColor = Color.valueOf("#111928");
    }

    public Label createTitle(String titleText) {
        // Create a new Label with the specified text and style
        return new Label(titleText, titleStyle);
    }

    public Container<Label> createDescription(String descriptionText) {
        Label.LabelStyle descriptionStyle = new Label.LabelStyle();
        descriptionStyle.font = FontCollection.getFont(ViewState.getScaledLength(65));
        descriptionStyle.fontColor = Color.valueOf("#111928");

        Label description = new Label(descriptionText, descriptionStyle);
        description.setWrap(true);
        Container<Label> container = new Container<Label>(description);
        container.width(ViewState.getScaledLength(1200));
        return container;
    }
}
