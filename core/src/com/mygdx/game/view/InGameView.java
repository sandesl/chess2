package com.mygdx.game.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.GameManager;
import com.mygdx.game.controller.InGameController;
import com.mygdx.game.model.Game;
import com.mygdx.game.model.GameStatus;
import com.mygdx.game.model.Piece;
import com.mygdx.game.model.Position;
import com.mygdx.game.model.board.Board;
import com.mygdx.game.model.board.Move;
import com.mygdx.game.model.board.Square;
import com.mygdx.game.model.player.Player;
import com.mygdx.game.view.gestureListeners.InGameGestureListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class InGameView extends ViewState {

    private Table boardTable;
    private final Texture tileTexture1 = new Texture("green.png");
    private final Texture tileTexture2 = new Texture("white.jpg");
    private final Texture tileTexture3 = new Texture("red.jpg");

    private Square selectedPieceSquare = null;

    private final BoundedCamera camera = new BoundedCamera();

    private final int squareSize;
    private final Viewport viewport;

    private final InGameHUD topHUD;

    public Board getViewedMoveHistoryBoard() {
        return viewedMoveHistoryBoard;
    }

    public void setViewedMoveHistoryBoard(Board viewedMoveHistoryBoard) {
        this.viewedMoveHistoryBoard = viewedMoveHistoryBoard;
    }

    private Board viewedMoveHistoryBoard;
    private Boolean isViewingMoveHistory = false;
    private Move selectedMove;

    public Move getSelectedMove() {
        return selectedMove;
    }

    public void setSelectedMove(Move selectedMove) {
        this.selectedMove = selectedMove;
    }

    public Boolean getViewingMoveHistory() {
        return isViewingMoveHistory;
    }

    public void setViewingMoveHistory(Boolean viewingMoveHistory) {
        isViewingMoveHistory = viewingMoveHistory;
    }

    public InGameView(ViewStateManager viewStateManager) {
        super(viewStateManager);
        stage = new Stage();
        // Set stage camera to the bounded camera
        stage.getViewport().setCamera(camera);

        // Calculate viewport size
        int viewportWidth = Gdx.graphics.getWidth();
        int viewportHeight = (Gdx.graphics.getHeight() / 4) * 2;
        // Get board size
        Board board = InGameController.getBoard();
        int boardWidth = board.getWidth();
        int boardHeight = board.getHeight();
        // Calculate square size
        squareSize = viewportWidth / boardWidth;
        System.out.println("Viewport width: " + viewportWidth + " Viewport height: " + viewportHeight);
        camera.setToOrtho(false, viewportWidth, viewportHeight);
        // Set camera bounds
        camera.setWorldBounds(0, 0, viewportWidth, viewportHeight);
        viewport = new FitViewport(viewportWidth, viewportHeight, camera);
        viewport.update(viewportWidth, viewportHeight, true);
        viewport.setScreenPosition(0, Gdx.graphics.getHeight()/4);
        topHUD = new InGameHUD(viewport, InGameHUD.HUDPosition.TOP, this);

        System.out.println("viewport top gutter height: " + viewport.getTopGutterHeight());
        stage = new Stage(viewport);

        InputMultiplexer inputMultiplexer = new InputMultiplexer();
        inputMultiplexer.addProcessor(new GestureDetector(new InGameGestureListener(this)));
        inputMultiplexer.addProcessor(stage);
        inputMultiplexer.addProcessor(topHUD.getStage());
        Gdx.input.setInputProcessor(inputMultiplexer);

        topHUD.update();
        update();
    }

    public Viewport getViewport() {
        return viewport;
    }


    @Override
    public void update() {
        stage.clear();
        topHUD.update();

        if (InGameController.getGameStatus() == GameStatus.CHECKMATE) {
            InGameController.setEndGameState();
            return;
        }

        boardTable = new Table();
        boardTable.setFillParent(true);
        boardTable.setDebug(false);
        Board board;
        if (isViewingMoveHistory) {
            board = viewedMoveHistoryBoard;
        } else {
            board = InGameController.getBoard();
        }
        stage.addActor(boardTable);

        int boardWidth = board.getWidth();
        int boardHeight = board.getHeight();

        // Create the board
        for (int h = 0; h < boardHeight; h++) {
            for (int w = 0; w < boardWidth; w++) {
                // Create a stack for each tile
                Stack stack = new Stack();
                final int posX = w;
                final int posY = h;
                // Create a tile and add it to the stack
                Image tileImage;

                if (board.getSquare(posX, posY).getIsHighlighted()) {
                    tileImage = new Image(tileTexture3);
                }
                else {
                    tileImage = ((h + w) % 2 == 0) ? new Image(tileTexture1) : new Image(tileTexture2);
                }
                if (selectedMove != null) {
                    if (board.getSquare(posX, posY).getPosition().equals(selectedMove.getFromPosition()) || board.getSquare(posX, posY).getPosition().equals(selectedMove.getToPosition())) {
                        tileImage = new Image(tileTexture3);
                    }
                }
                stack.add(tileImage);

                InGameController.SetSquareHighlight(new Position(posX, posY), false);


                // Add piece to stack if square is occupied
                if (board.getSquare(posX, posY).isOccupiedByPiece()) {
                    Piece piece = board.getSquare(posX, posY).getPiece();
                    Image pieceImage = piece.getImage();
                    stack.add(pieceImage);

                    if (pieceImage.getListeners() != null) {
                        pieceImage.clearListeners();
                    }
                    // Add listener to piece
                    if (!isViewingMoveHistory) {
                        pieceImage.addListener(new InputListener() {
                            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                                touchPieceListener(board, posX, posY);
                                return true;
                            }
                        });
                    }

                }

                // Add stack to board
                boardTable.add(stack).size(squareSize, squareSize);

                if (!isViewingMoveHistory) {
                    tileImage.addListener(new InputListener() {
                        public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                            touchSquareListener(board, posX, posY);
                            return true;
                        }
                    });
                }

            }
            // Add a new row for each row of tiles
            boardTable.row();
        }
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        viewport.apply(); // Switch back to game viewport
        stage.act();
        stage.draw();
        topHUD.apply(); // Switch to HUD viewport
        topHUD.render();
    }

    private void touchSquareListener(Board board, int posX, int posY) {
        Square clickedSquare = board.getSquare(posX, posY);
        if (selectedPieceSquare != null) {
            InGameController.MovePiece(selectedPieceSquare.getPiece(), clickedSquare);
        }
        selectedPieceSquare = null;
        update();
    }

    private void touchPieceListener(Board board, int posX, int posY) {
        Square clickedSquare = board.getSquare(posX, posY);
        Piece clickedSquarePiece = clickedSquare.getPiece();
        Piece pieceOnTile = clickedSquare.getPiece();
        Boolean isPlayersTurn = InGameController.isColorsTurn(pieceOnTile.getColor());

        // Check if its the players turn or a piece is selected
        if (isPlayersTurn || selectedPieceSquare != null) {

            // capture piece
            if (selectedPieceSquare != null) {
                if (selectedPieceSquare.getPiece().getColor() != clickedSquarePiece.getColor()) {
                    InGameController.MovePiece(selectedPieceSquare.getPiece(), clickedSquare);
                    selectedPieceSquare = null;
                    update();
                    return;
                }
            }

            // check if clicking the same square
            if (selectedPieceSquare == clickedSquare) {
                selectedPieceSquare = null;
            }
            // select a piece if no piece is selected
            else if (selectedPieceSquare == null) {
                selectedPieceSquare = clickedSquare;
                for (Position validMove : pieceOnTile.getValidMoves()) {
                    InGameController.SetSquareHighlight(validMove, true);
                }
            } else {
                selectedPieceSquare = clickedSquare;
                for (Position validMove : pieceOnTile.getValidMoves()) {
                    InGameController.SetSquareHighlight(validMove, true);
                }
            }


        }
        update();
    }

    float minZoom = 0.1f;
    float maxZoom = 1000.0f;
    public void zoom(float distance) {
        camera.zoom = MathUtils.clamp(camera.zoom + distance * 0.0001f, minZoom, maxZoom);
        System.out.println("Zoom" + camera.zoom);
    }

    public void pan(float deltaX, float deltaY) {
        camera.translateSafe(-deltaX, deltaY);
    }
}

class InGameHUD {
    OrthographicCamera camera;
    Stage stage;
    Table table = new Table();
    Label.LabelStyle labelStyle = new Label.LabelStyle(FontCollection.getFont(54), Color.BLACK);
    Label currentPlayerLabel = new Label("Current player: ", labelStyle);
    HashMap<Player, Label> playerTimeLabels = new HashMap<>();

    TextButton pauseButton;
    boolean isTimerPaused = false; // To track timer state


    ArrayList<Move> moves;

    enum HUDPosition {
        TOP,
        BOTTOM
    }

    InGameView inGameView;

    public InGameHUD(Viewport inGameViewport, HUDPosition position, InGameView inGameView){
        System.out.println("inGameViewPort.getWorldWidth " + inGameViewport.getWorldWidth());
        System.out.println("inGameViewPort.getTopGutterHeight " + inGameViewport.getTopGutterHeight());
        System.out.println("inGameViewPort.getTopGutterY " + inGameViewport.getTopGutterY());
        System.out.println("inGameViewPort.getScreenWidth " + inGameViewport.getScreenWidth());
        System.out.println("inGameViewPort.getScreenHeight " + inGameViewport.getScreenHeight());
        System.out.println("Gdx.graphics.getWidth() " + Gdx.graphics.getWidth());
        System.out.println("Gdx.graphics.getHeight() " + Gdx.graphics.getHeight());

        this.inGameView = inGameView;
        float worldWidth;
        float worldHeight;
        int screenX;
        int screenY;
        switch (position) {
            case TOP:
                worldWidth = inGameViewport.getWorldWidth();
                worldHeight = inGameViewport.getTopGutterHeight();
                screenX = 0;
                screenY = Gdx.graphics.getHeight() - inGameViewport.getTopGutterHeight();
                camera = new OrthographicCamera(worldWidth, worldHeight);
                camera.setToOrtho(false);
                camera.update();
                stage = new Stage(new FitViewport(worldWidth, worldHeight, camera));
                break;
            case BOTTOM:
                worldWidth = inGameViewport.getWorldWidth();
                worldHeight = inGameViewport.getBottomGutterHeight();
                screenX = 0;
                screenY = 0;
                camera = new OrthographicCamera(worldWidth, worldHeight);
                camera.setToOrtho(false);
                stage = new Stage(new FitViewport(worldWidth, worldHeight, camera));
                break;
            default:
                throw new IllegalArgumentException("Invalid HUD position");
        }

        stage.getViewport().setScreenPosition(screenX, screenY);
        table.setFillParent(true);
        //set table to start from top of screen
        table.top();
        table.setDebug(false);
        initializePlayerLabels();
        stage.addActor(table);


    }


    private void addPauseButton(){
        TextureAtlas atlas = new TextureAtlas(Gdx.files.internal("data/uiskin.atlas"));
        TextureAtlas.AtlasRegion buttonTexture = atlas.findRegion("default-rect");
        TextButton.TextButtonStyle textButtonStyle = new TextButton.TextButtonStyle();
        textButtonStyle.font = FontCollection.getFont(60);
        textButtonStyle.up = new TextureRegionDrawable(buttonTexture);
        textButtonStyle.down = new TextureRegionDrawable(buttonTexture);
        pauseButton = new TextButton("Pause", textButtonStyle);

        pauseButton.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                isTimerPaused = !isTimerPaused;
                Game.getInstance().togglePause(isTimerPaused);
                pauseButton.setText(isTimerPaused ? "Resume" : "Pause");
                return true;
            }
        });

        TextButton forfeitButton = new TextButton("Forfeit", textButtonStyle);
        forfeitButton.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                InGameController.forfeitGame();
                return true;
            }
        });
        table.row();
        Table buttonTable = new Table();
        buttonTable.add(forfeitButton).pad(5);
        buttonTable.add(pauseButton).pad(5);
        table.add(buttonTable).growX();
        table.row();
    }


    public void initializePlayerLabels() {
        Table playerTimeTable = new Table();
        List<Player> players = Game.getInstance().getPlayers(); // Assuming this returns a list of players
        Player player1 = players.get(0);
        Player player2 = players.get(1);
        Label time1 = new Label(player1.getName() + " " + player1.getRemainingTimeString() + " | ", labelStyle); // Initial time can be set as needed
        playerTimeLabels.put(player1, time1); // Put the player and label into the map
        Label time2 = new Label(player2.getRemainingTimeString() + " " + player2.getName() , labelStyle);
        playerTimeLabels.put(player2, time2);

        playerTimeTable.add(time1);
        playerTimeTable.add(time2);
        table.add(playerTimeTable).pad(5).growX();
    }

    private void updatePlayerTimeLabels() {
        if (Game.getInstance().getGameStatus() == GameStatus.CHECKMATE) {
            GameManager.getInstance().setStateManagers(GameManager.GameState.END_GAME);
            return;
        }
        Player player = Game.getInstance().getCurrentPlayer();
        if (player != null) {
            Label timeLabel = playerTimeLabels.get(player);
            if (timeLabel != null) {
                if ( player.getColor() == Piece.ChessPieceColor.WHITE) {
                    timeLabel.setText(player.getName() + " " + player.getRemainingTimeString() + " | ");
                } else {
                    timeLabel.setText(player.getRemainingTimeString() + " " + player.getName());
                }
            }
        }
    }



    public Stage getStage() {
        return stage;
    }

    public void apply() {
        stage.getViewport().apply();
    }

    public OrthographicCamera getCamera() {
        return camera;
    }

    public void render() {
        stage.act();
        updatePlayerTimeLabels();
        stage.draw();
    }

    public void update() {
        stage.clear();
        table.clear();
        currentPlayerLabel.setText("Current player: " + Game.getInstance().getCurrentPlayer().getName() + " (" + Game.getInstance().getCurrentPlayer().getColor().toString() + ")");
        table.add(currentPlayerLabel).pad(5);
        table.row();
        initializePlayerLabels();
        addPauseButton();
        table.add(new Label("Moves: ", labelStyle)).pad(5);
        table.row();
        Table moveTable = new Table();
        moveTable.setDebug(false);
        moves = Game.getInstance().getMoves();
        for (int i = 0; i < moves.size(); i++) {
            Move move = moves.get(i);
            Label moveLabel = new Label(move.toString(), labelStyle);
            int finalI = i;
            moveLabel.addListener(new InputListener() {
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    if (finalI == moves.size() - 1) {
                        inGameView.setViewingMoveHistory(false);
                        inGameView.setSelectedMove(null);
                        inGameView.update();
                        return false;
                    }
                    inGameView.setViewedMoveHistoryBoard(move.getBoardState());
                    inGameView.setViewingMoveHistory(true);
                    inGameView.setSelectedMove(move);
                    inGameView.update();
                    return false;
                }
            });
            moveTable.add(moveLabel).pad(10);

        }
        ScrollPane scrollPane = new ScrollPane(moveTable);
        //scrollPane.setFillParent(true);
        table.add(scrollPane);

        stage.addActor(table);
    }
}