package com.mygdx.game.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.mygdx.game.controller.HowToPlayController;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.graphics.Texture;

public class HowToPlayView extends ViewState {
    public HowToPlayView(ViewStateManager viewStateManager) {
        super(viewStateManager);
        setupUI();
    }

    private final String[] pages = {
            "Welcome to Chess Evolution! This is a new chess game where you can choose between 3 different loadouts and 3 different mapsizes. You can start by clicking \"Create Game\".",
            "Here you can choose between 3 loadouts: Standard, Medival and Modern. Where each loadout has there own pieces.",
            "There are tree new pieces to the game: Snake, Phenoix and Thief.",
            "This is the piece Phenoix, as you can see it can only move like a box around it.",
            "This is the piece Snake, this piece work like a bishop, but it jumps diagonaly instead.",
            "And last new piece is the Thief, this piece can move like a rook, but it jumps like the snake.",
            "After choosing loadout and mapsize, you can click \"Start Game\" to start the game.",
            "To win the game, you need to capture your opponent's king. Good luck!"
    };

    private final String[] imgUrls = { "images/howtoplay-1.png", "images/howtoplay-2.png", "images/howtoplay-3.png",
            "images/howtoplay-4.png", "images/howtoplay-5.png", "images/howtoplay-6.png", "images/howtoplay-7.png" };

    private final Table table = new Table();

    private void setupUI() {
        HowToPlayController.setMaxPages(pages.length);
        table.setFillParent(true);
        stage.addActor(table);
        HowToPlayController.reset();

        createBackButton(HowToPlayController::returnToMainMenu);
        addTitle(table);
        addDescription(table);
        addImg(table);
        addButtons(table);
    }

    private void addImg(Table table) {
        try {
            Texture logoTexture = new Texture(Gdx.files.internal(getImgUrl()));
            Image logo = new Image(logoTexture);
            table.add(logo).center();
        }
        // Catch exception if image index is out of bounds or image is not found
        // Simply do not add the image in this case
        catch (Exception e) {
        }
        table.row();
    }

    private void addTitle(Table table) {
        Label title = titleComponent.createTitle("How to Play");
        table.add(title).padTop(ViewState.getScaledLength(50)).padBottom(ViewState.getScaledLength(40));
        table.row();
    }

    private void addDescription(Table table) {
        Container<Label> title = titleComponent.createDescription(getDescription());
        table.add(title).padTop(ViewState.getScaledLength(40)).padBottom(ViewState.getScaledLength(60));
        table.row();
    }

    private String getImgUrl() {
        int index = HowToPlayController.getCurrentPage() - 1;
        if (index < 0 || index >= imgUrls.length) {
            throw new RuntimeException("Invalid index");
        }
        return imgUrls[index];
    }

    private String getDescription() {
        return pages[HowToPlayController.getCurrentPage() - 1];
    }

    private void addButtons(Table table) {
        Runnable nextPage = HowToPlayController::nextPage;
        Runnable previousPage = HowToPlayController::previousPage;

        int currentPage = HowToPlayController.getCurrentPage();
        int maxPages = HowToPlayController.getMaxPages();

        Object[][] buttons = {
                { "Previous", previousPage, "default", currentPage > 1 },
                { "Next", nextPage, "green", currentPage < maxPages },
        };

        for (Object[] buttonInfo : buttons) {
            String label = (String) buttonInfo[0];
            Runnable action = (Runnable) buttonInfo[1];
            String buttonType = (String) buttonInfo[2];
            boolean showButton = (boolean) buttonInfo[3];

            if (showButton) {
                InputListener listener = createButtonListener(action);
                TextButton button;

                if (buttonType.equals("green")) {
                    button = buttonComponent.createGreenButton(label, listener);
                } else {
                    button = buttonComponent.createDefaultButton(label, listener);
                }

                int BUTTON_GAP = 25;
                table.add(button).padBottom(BUTTON_GAP).padTop(BUTTON_GAP).fillX().uniformX();
                table.row();
            }
        }
    }

    // Should be called by the outside view manager
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    public void dispose() {
        stage.dispose();
    }

    @Override
    public void update() {
        updateImageAndDescription();
    }

    private void updateImageAndDescription() {
        table.clearChildren();

        createBackButton(HowToPlayController::returnToMainMenu);
        addTitle(table);
        addDescription(table);
        addImg(table);
        addButtons(table);
    }

}
