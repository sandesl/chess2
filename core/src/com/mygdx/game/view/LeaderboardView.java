package com.mygdx.game.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.mygdx.game.controller.LeaderboardController;
import com.mygdx.game.service.data.UserProfileData;

import java.util.List;

public class LeaderboardView extends ViewState {

    private static Table leaderboardTable;
    private static Label loadingLabel;

    public LeaderboardView(ViewStateManager viewStateManager) {
        super(viewStateManager);
        setupStage();
        setupUI();
    }

    private void setupUI() {
        Table mainTable = new Table();
        mainTable.setFillParent(true);
        stage.addActor(mainTable);

        mainTable.top().padTop(ViewState.getScaledLength(100));

        addTitle(mainTable);

        loadingLabel = textComponent.createTitle("Loading...");
        mainTable.add(loadingLabel).colspan(3);

        leaderboardTable = new Table();
        leaderboardTable.top();

        ScrollPane scrollPane = new ScrollPane(leaderboardTable);
        scrollPane.setScrollingDisabled(true, false);
        scrollPane.setFadeScrollBars(false);
        scrollPane.setScrollBarPositions(false, true);

        mainTable.row();
        mainTable.add(scrollPane).expand().fill().colspan(3);
        mainTable.row();

        createBackButton(LeaderboardController::returnToMenu);
    }

    private void addTitle(Table table) {
        Label title = titleComponent.createTitle("Leaderboard");
        table.add(title).colspan(3).center().padTop(ViewState.getScaledLength(140)).padBottom(ViewState.getScaledLength(10));
        table.row();
    }

    public static void updateLeaderboard(List<UserProfileData> leaderboardData) {
        Gdx.app.postRunnable(() -> {
            leaderboardTable.clear();
            if (leaderboardData.isEmpty()) {
                loadingLabel.setText("No data available.");
                return;
            }

            loadingLabel.setVisible(false);
            addLeaderboardHeaders();
            int rank = 1;
            for (UserProfileData data : leaderboardData) {
                String elo = data.getElo() == null ? "0" : String.valueOf(data.getElo());
                String[] entry = {String.valueOf(rank), data.getDisplayName(), elo};
                if (entry.length == 3) {
                    addLeaderboardRow(entry);
                }
                rank++;
            }
        });
    }

    private static void addLeaderboardHeaders() {
        String[] headers = {"Rank", "Name", "Rating"};
        for (String header : headers) {
            Label headerLabel = textComponent.createTitle(header);
            leaderboardTable.add(headerLabel).expandX().fillX().padLeft(50);
        }
        leaderboardTable.row();
    }

    private static void addLeaderboardRow(String[] rowData) {
        for (String data : rowData) {
            Label title = textComponent.createTitle(data);
            leaderboardTable.add(title).expandX().fillX().padLeft(50);
        }
        leaderboardTable.row();
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    @Override
    public void update() {
        // Leaderboard view doesn't require update logic in this case
    }
}
