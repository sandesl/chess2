package com.mygdx.game.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.Scaling;
import com.mygdx.game.controller.MainMenuController;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.graphics.Texture;

public class MainMenuView extends ViewState {
    public MainMenuView(ViewStateManager viewStateManager) {
        super(viewStateManager);
        setupUI();
    }

    private void setupUI() {
        Table table = new Table();
        table.setFillParent(true);
        stage.addActor(table);

        addLogo(table);
        addTitle(table);
        addButtonComponents(table);
    }

    private void addLogo(Table table) {
        // Load the texture for the logo
        Texture logoTexture = new Texture(Gdx.files.internal("images/logo.png"));

        Image logo = new Image(logoTexture);
        logo.setScaling(Scaling.fit);

        table.add(logo).center();
        table.row();
    }

    private void addTitle(Table table) {
        Label title = titleComponent.createTitle("Chess Evolution");
        table.add(title).padTop(ViewState.getScaledLength(150)).padBottom(ViewState.getScaledLength(150));
        table.row();
    }

    private void addButtonComponents(Table table) {
        // Define actions for each button
        Runnable createGameAction = MainMenuController::startGameLobby;
        Runnable leaderboardAction = MainMenuController::menuLeaderboard;
        Runnable howToPlayAction = MainMenuController::menuHowToPlay;
        Runnable settingsAction = MainMenuController::menuSettings;

        Object[][] buttons = {
                { "Create Game", createGameAction, "green" },
                { "Leaderboard", leaderboardAction, "default" },
                { "How To Play", howToPlayAction, "default" },
                { "Settings", settingsAction, "default" }
        };

        for (Object[] buttonInfo : buttons) {
            String label = (String) buttonInfo[0];
            Runnable action = (Runnable) buttonInfo[1];
            String buttonType = (String) buttonInfo[2];

            // Create a listener for the current button's action
            InputListener listener = createButtonListener(action);
            TextButton button;

            if (buttonType.equals("green")) {
                button = buttonComponent.createGreenButton(label, listener);
            } else {
                button = buttonComponent.createDefaultButton(label, listener);
            }

            int BUTTON_GAP = ViewState.getScaledLength(25);
            table.add(button).padBottom(BUTTON_GAP).padTop(BUTTON_GAP).fillX().uniformX();
            table.row();
        }
    }

    // Should be called by the outside view manager
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    public void dispose() {
        stage.dispose();
    }

    @Override
    public void update() {
        // main menu doesnt update anything
    }

}