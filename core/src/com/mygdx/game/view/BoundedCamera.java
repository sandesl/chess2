package com.mygdx.game.view;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;

public class BoundedCamera extends OrthographicCamera {
    BoundingBox left, right, top, bottom = null;

    BoundingBox full;

    public void setWorldBounds(int left, int bottom, int width, int height) {
        int top = bottom + height;
        int right = left + width;

        this.left = new BoundingBox(new Vector3(left - 2, 0, 0), new Vector3(left -1, top, 0));
        this.right = new BoundingBox(new Vector3(right + 1, 0, 0), new Vector3(right + 2, top, 0));
        this.top = new BoundingBox(new Vector3(0, top + 1, 0), new Vector3(right, top + 2, 0));
        this.bottom = new BoundingBox(new Vector3(0, bottom - 1, 0), new Vector3(right, bottom - 2, 0));

        this.full = new BoundingBox(new Vector3(left, bottom, 0), new Vector3(right, top, 0));
    }

    Vector3 lastPosition = new Vector3();
    @Override
    public void translate(float x, float y) {
        lastPosition.set(position.x, position.y, 0);
        super.translate(x, y);
    }

    public void translateSafe(float x, float y) {
        translate(x, y);
        update();
        ensureBounds();
        update();
    }

    public void ensureBounds() {
        if (!full.contains(position)) {
            position.set(lastPosition);
        }
    }
}
