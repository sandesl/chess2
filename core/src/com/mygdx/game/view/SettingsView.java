package com.mygdx.game.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.mygdx.game.controller.SettingsController;

public class SettingsView extends ViewState {

    public SettingsView(ViewStateManager viewStateManager) {
        super(viewStateManager);
        setupUI();
    }

    private void setupUI() {
        Table table = new Table();
        table.setFillParent(true);
        stage.addActor(table);

        addLogo(table);
        addTitle(table);
        addSettingsComponents(table);
        createBackButton(SettingsController::returnToMainMenu);
    }

    private void addLogo(Table table) {
        Texture logoTexture = new Texture(Gdx.files.internal("images/logo.png"));
        Image logo = new Image(logoTexture);
        table.add(logo).center();
        table.row();
    }

    private void addTitle(Table table) {
        Label title = titleComponent.createTitle("Settings");
        table.add(title).padTop(ViewState.getScaledLength(150)).padBottom(ViewState.getScaledLength(150));
        table.row();
    }

    private void addSettingsComponents(Table table) {
        CheckBox musicCheckBox = checkboxComponent.createCheckbox("Music", !SettingsController.isMusicMuted(),
                createButtonListener(SettingsController::toggleMusic));
        CheckBox soundCheckBox = checkboxComponent.createCheckbox("Sound Effects", !SettingsController.isSoundMuted(),
                createButtonListener(SettingsController::toggleSoundEffects));

        table.add(musicCheckBox).padBottom(ViewState.getScaledLength(25)).fillX().uniformX();
        table.row();
        table.add(soundCheckBox).padBottom(ViewState.getScaledLength(100)).fillX().uniformX();
    }

    public void dispose() {
        stage.dispose();
    }

    @Override
    public void update() {
        // main menu doesnt update anything
    }

}
