package com.mygdx.game.view;

import java.util.Stack;

public class ViewStateManager {

    // View state manager is a stack of view states
    // The top of the stack is the current view state
    // The current view state is the one that is rendered and updated
    // The stack is used to manage the view states

    private final Stack<ViewState> views;

    public ViewStateManager() {
        views = new Stack<ViewState>();
    }

    public void push(ViewState view) {
        views.push(view);
    }

    public void pop() {
        views.pop();
    }

    public void set(ViewState view) {
        views.pop();
        views.push(view);
    }

    public void render() {
        views.peek().render();
    }

    public void update() {
        views.peek().update();
    }

    public void dispose() {
        for (ViewState view : views) {
            view.dispose();
        }
    }

    public int size() {
        return views.size();
    }

    public void resize(int width, int height) {
        views.peek().resize(width, height);
    }
}
