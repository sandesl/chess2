package com.mygdx.game.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.mygdx.game.ChessGame;
import com.mygdx.game.view.components.ButtonComponent;
import com.mygdx.game.view.components.CheckboxComponent;
import com.mygdx.game.view.components.TextInputComponent;
import com.mygdx.game.view.components.SelectComponent;
import com.mygdx.game.view.components.TextComponent;
import com.mygdx.game.view.components.TitleComponent;

public abstract class ViewState {

    // ViewState is an abstract class that is extended by other views
    // It contains the camera and font that are used by all views (Subject to chane
    // depending on game design)
    // It also contains the ViewStateManager that is used to manage the views

    protected ViewStateManager viewStateManager;
    protected OrthographicCamera camera;
    protected SpriteBatch batch;
    protected Stage stage;
    protected TextureAtlas atlas;
    protected TextureAtlas.AtlasRegion buttonTexture;
    protected Label.LabelStyle labelStyle;
    protected ButtonComponent buttonComponent;
    protected CheckboxComponent checkboxComponent;
    protected TextInputComponent textInputComponent;
    protected TitleComponent titleComponent;
    protected SelectComponent selectComponent;
    protected static TextComponent textComponent;

    private static final float DESIGN_WIDTH = 1440;
    protected static float scaleFactor = Gdx.graphics.getWidth() / DESIGN_WIDTH;
    protected Skin skin;

    public ViewState(ViewStateManager viewStateManager) {
        this.viewStateManager = viewStateManager;
        camera = new OrthographicCamera();
        camera.setToOrtho(false, ChessGame.WIDTH, ChessGame.HEIGHT);
        batch = new SpriteBatch();
        batch.setProjectionMatrix(camera.combined);

        atlas = new TextureAtlas(Gdx.files.internal("data/uiskin.atlas"));
        buttonTexture = atlas.findRegion("default-rect");
        skin = new Skin(Gdx.files.internal("uiskin.json"));

        buttonComponent = new ButtonComponent();
        checkboxComponent = new CheckboxComponent();
        textInputComponent = new TextInputComponent(skin);
        titleComponent = new TitleComponent();
        selectComponent = new SelectComponent(skin);
        textComponent = new TextComponent();

        labelStyle = new Label.LabelStyle(FontCollection.getFont(72), Color.BLACK);

        stage = new Stage(new FitViewport(ChessGame.WIDTH, ChessGame.HEIGHT, camera));

        scaleFactor = Gdx.graphics.getWidth() / DESIGN_WIDTH;
        setupStage();
    }

    public static float getScaleFactor() {
        return scaleFactor;
    }

    public static int getScaledLength(int length) {
        return (int) (length * scaleFactor);
    }

    protected void setupStage() {
        stage = new Stage(new StretchViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight()));
        Gdx.input.setInputProcessor(stage);
    }

    public void render() {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();
    }

    protected void createBackButton(Runnable action) {
        Gdx.input.setInputProcessor(stage);
        TextButton backButton = buttonComponent.createDefaultButton("Back", createButtonListener(action));

        int backButtonMargin = (int) (50 * getScaleFactor());
        int backButtonWidth = (int) (300 * getScaleFactor());

        backButton.setSize(backButtonWidth, backButton.getHeight());

        backButton.setPosition(backButtonMargin, Gdx.graphics.getHeight() - backButton.getHeight() - backButtonMargin);
        stage.addActor(backButton);
    }

    public abstract void update();

    public void dispose() {
        stage.dispose();
        batch.dispose();
    }

    protected InputListener createButtonListener(Runnable action) {
        return new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                action.run();
                viewStateManager.update();
                return true;
            }
        };
    }

    protected void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

}
