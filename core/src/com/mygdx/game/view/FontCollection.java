package com.mygdx.game.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

import java.util.HashMap;

public class FontCollection {
    private static FontCollection instance;

    // A hashmap to store the fonts generated
    private static final HashMap<Integer, BitmapFont> fonts = new HashMap<>();

    static private void checkExistenceCreateIfNot() {
        if (instance == null) {
            instance = new FontCollection();
        }
    }

    // Returns a BitmapFont of the specified size, if it doesn't exist it creates it
    static public BitmapFont getFont(int size) {
        checkExistenceCreateIfNot();

        if (!fonts.containsKey(size)) {
            FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/roboto/Roboto-Regular.ttf"));
            FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
            parameter.size = size;
            fonts.put(size, generator.generateFont(parameter));
            generator.dispose();
        }
        return fonts.get(size);

    }


    private FontCollection() {
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/roboto/Roboto-Regular.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        // Generate size 72, 48, 36, 24, 12 fonts
        parameter.size = 72;
        fonts.put(72, generator.generateFont(parameter));
        parameter.size = 48;
        fonts.put(48, generator.generateFont(parameter));
        parameter.size = 36;
        fonts.put(36, generator.generateFont(parameter));
        parameter.size = 24;
        fonts.put(24, generator.generateFont(parameter));
        parameter.size = 12;
        fonts.put(12, generator.generateFont(parameter));
        generator.dispose(); // don't forget to dispose to avoid memory leaks!
    }


    public void dispose() {
    }

}
