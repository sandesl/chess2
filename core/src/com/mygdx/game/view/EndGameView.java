package com.mygdx.game.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.mygdx.game.GameManager;
import com.mygdx.game.controller.EndGameController;
import com.mygdx.game.model.player.Player;

public class EndGameView extends ViewState {
    public EndGameView(ViewStateManager viewStateManager) {
        super(viewStateManager);
        setupStage();
        Table table = new Table();
        table.setFillParent(true);
        stage.addActor(table);

        Player winner = EndGameController.getWinner();
        Player loser = EndGameController.getLoser();

        winner = GameManager.getInstance().findPlayerByName(winner.getName());
        loser = GameManager.getInstance().findPlayerByName(loser.getName());

        table.row();
        table.add(titleComponent.createTitle("Match ended"));
        table.row();
        table.add(titleComponent.createTitle("Winner: " + winner.getName()));
        table.row();
        table.add(titleComponent.createTitle("Loser: " + loser.getName()));
        table.row();
        table.add(buttonComponent.createDefaultButton("Main Menu", createButtonListener(EndGameController::returnToMainMenu)));

        Gdx.input.setInputProcessor(stage);
    }
    @Override
    public void update() {

    }
}
