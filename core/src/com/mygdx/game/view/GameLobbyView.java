package com.mygdx.game.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.utils.Align;
import com.mygdx.game.controller.GameLobbyController;
import com.mygdx.game.model.player.Player;

import java.util.ArrayList;
import java.util.Objects;

public class GameLobbyView extends ViewState {

    private TextField player1NameField, player2NameField;
    private SelectBox<String> timePerMoveSelect, mapSizeSelect, player1LoadoutSelect, player2LoadoutSelect;
    private Label errorMessage;

    public GameLobbyView(ViewStateManager viewStateManager) {
        super(viewStateManager);
        createBackButton(GameLobbyController::returnToMainMenu);

        setupUI();
        Gdx.input.setInputProcessor(stage);
    }

    private void setupUI() {
        Table table = new Table();
        table.setFillParent(true);
        stage.addActor(table);

        table.add(titleComponent.createTitle("Create Match")).padBottom(20);
        table.row();

        if (GameLobbyController.getBoardSizes() == null || GameLobbyController.getTimeLimits() == null
                || GameLobbyController.getLoadouts() == null) {
            Gdx.app.log("GameLobbyView", "Configuration data is not loaded");
        }

        String[] boardSizeOptions = GameLobbyController.getBoardSizes().stream()
                .map(size -> size + "x" + size)
                .toArray(String[]::new);
        String[] timeOptions = GameLobbyController.getTimeLimits().stream()
                .map(time -> time + "s")
                .toArray(String[]::new);
        String[] loadoutOptions = GameLobbyController.getLoadouts().toArray(new String[0]);

        timePerMoveSelect = createSelectBox("Total time", timeOptions, table);
        mapSizeSelect = createSelectBox("Map Size", boardSizeOptions, table);

        player1LoadoutSelect = createSelectBox("White Player Loadout", loadoutOptions, table);
        player2LoadoutSelect = createSelectBox("Black Player Loadout", loadoutOptions, table);

        player1NameField = createTextField("White Player Name:", table);
        player2NameField = createTextField("Black Player Name:", table);


        errorMessage = new Label("", skin);
        errorMessage.setColor(1, 0, 0, 1);
        errorMessage.setVisible(false);
        table.add(errorMessage).colspan(2).padTop(10);
        table.row();

        table.add(createStartGameButton()).padTop(20);
    }

    private Label createLabel(String text) {
        Label.LabelStyle labelStyle = new Label.LabelStyle();
        labelStyle.font = FontCollection.getFont(ViewState.getScaledLength(45));
        labelStyle.fontColor = Color.valueOf("#111928");

        return new Label(text, labelStyle);
    }

    private SelectBox<String> createSelectBox(String label, String[] options, Table table) {
        table.add(createLabel(label));
        table.row();
        SelectBox<String> selectBox = selectComponent.createSelectBox(options);
        Container<SelectBox<String>> container = new Container<SelectBox<String>>(selectBox);
        container.width(ViewState.getScaledLength(1200));
        container.height(ViewState.getScaledLength(100));
        table.add(container).padBottom(ViewState.getScaledLength(50));
        table.row();
        return selectBox;
    }

    private TextField createTextField(String nameLabel, Table table) {
        table.add(createLabel(nameLabel));
        table.row();
        TextField textField = textInputComponent.createTextField(nameLabel, null);
        
        Container<TextField> container = new Container<TextField>(textField);
        container.width(ViewState.getScaledLength(1200));
        container.height(ViewState.getScaledLength(100));
        table.add(container).padBottom(ViewState.getScaledLength(50));
        table.row();
        return textField;
    }

    private TextButton createStartGameButton() {
        return buttonComponent.createGreenButton("Start Match", createButtonListener(() -> {
            Gdx.input.setOnscreenKeyboardVisible(false);

            if (player1NameField.getText().trim().isEmpty() || player2NameField.getText().trim().isEmpty()) {
                errorMessage.setText("Please enter a name for both players");
                errorMessage.setVisible(true);
                return;
            }

            if (Objects.equals(player1NameField.getText().trim(), player2NameField.getText().trim())) {
                errorMessage.setText("Player names must be unique");
                errorMessage.setVisible(true);
                return;
            }

            ArrayList<Player> playersInLobby = new ArrayList<>();
            playersInLobby.add(new Player(player1NameField.getText().toLowerCase()));
            playersInLobby.add(new Player(player2NameField.getText().toLowerCase()));

            int mapSize = Integer.parseInt(mapSizeSelect.getSelected().split("x")[0]);
            int timePerMoveInSeconds = Integer.parseInt(timePerMoveSelect.getSelected().replace("s", ""));

            ArrayList<String> loadouts = new ArrayList<>();
            loadouts.add(player2LoadoutSelect.getSelected());
            loadouts.add(player1LoadoutSelect.getSelected());

            GameLobbyController.startGame(playersInLobby, mapSize, timePerMoveInSeconds, loadouts);
        }));
    }

    @Override
    public void update() {
        // Doesn't update anything
    }
}
