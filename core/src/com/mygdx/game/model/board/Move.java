package com.mygdx.game.model.board;

import com.mygdx.game.model.Piece;
import com.mygdx.game.model.Position;

public class Move {
    private final Position fromPosition;
    private final Position toPosition;
    private final Piece pieceMoved;
    private final Piece pieceCaptured;
    private final Board boardState;

    public Move(Position fromPosition, Position toPosition, Piece pieceMoved, Piece pieceCaptured, Board boardState){
        this.fromPosition = fromPosition;
        this.toPosition = toPosition;
        this.pieceMoved = pieceMoved;
        this.pieceCaptured = pieceCaptured;
        this.boardState = boardState;
    }

    @Override
    public String toString() {
        // Generate chess notation for the move
        String pieceInitial = pieceMoved.getName().substring(0, 1).toUpperCase();
        String toPosition = this.toPosition.toString();
        return pieceInitial + toPosition;

    }

    public Board getBoardState() {
        return boardState;
    }

    public Position getFromPosition() {
        return fromPosition;
    }

    public Position getToPosition() {
        return toPosition;
    }
}
