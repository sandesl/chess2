package com.mygdx.game.model.board;

import com.mygdx.game.model.ChessPieceFactory;
import com.mygdx.game.model.Piece;
import com.mygdx.game.model.Position;
import com.sun.org.apache.xpath.internal.operations.Bool;

public class Square {
    private final Position position;
    private Piece piece;

    private boolean isOccupied;
    private Boolean isHighlighted = false;

    public Square(int x, int y) {
        this.piece = null;
        this.position = new Position(x, y);
    }

    public Square(Position position) {
        this.piece = null;
        this.position = position;
    }

    public Square(Square that) {
        this.position = new Position(that.getPosition().getX(), that.getPosition().getY());
        if (that.piece == null) {
            this.piece = null;
        } else {
            this.piece = ChessPieceFactory.createPiece(that.piece);
            this.piece.setSquare(this);
        }
        this.isOccupied = that.isOccupied;
        this.isHighlighted = that.isHighlighted;
    }

    public boolean isOccupied() {
        return isOccupied;
    }

    public boolean isOccupiedByPiece() {
        return isOccupied && this.piece != null;
    }

    public void setPiece(Piece piece) {
        this.piece = piece;
        isOccupied = true;
        piece.setSquare(this);
    }

    public void removePiece() {
        piece = null;
        isOccupied = false;
    }

    public void setOccupied(boolean isOccupied) {
        this.isOccupied = isOccupied;
        if (!isOccupied) {
            piece = null;
        }
    }

    public Piece getPiece() {
        return piece;
    }

    public int getX() {
        return position.getX();
    }

    public int getY() {
        return position.getY();
    }

    public Position getPosition() {
        return position;
    }

    public void setIsHighlighted(Boolean isHighlighted) {
        this.isHighlighted = isHighlighted;
    }
    public Boolean getIsHighlighted() {
        return isHighlighted;
    }

    private String getPieceNameIfNotNull() {
        return piece != null ? piece.getName() : "null";
    }

    @Override
    public String toString() {
        return "Square{" +
                "x=" + position.getX() +
                ", y=" + position.getY() +
                ", piece=" + getPieceNameIfNotNull() +
                ", isOccupied=" + isOccupied +
                ", canMoveTo=" + isHighlighted +
                '}';
    }

    public Boolean equals(Square that) {
        return this.position.equals(that.position);
    }
}
