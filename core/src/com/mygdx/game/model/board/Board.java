package com.mygdx.game.model.board;

import com.mygdx.game.model.Game;
import com.mygdx.game.model.Piece;
import com.mygdx.game.model.Position;
import com.mygdx.game.model.player.Loadout;

public class Board {
    private final int width;
    private final int height;

    private final Square[][] squares;

    public Board(int width, int height) {
        this.width = width;
        this.height = height;

        if (width < 8 || height < 8) {
            throw new IllegalArgumentException("Board must have a width and height of at least 8, and be an even number.");
        }

        if (width % 2 != 0 || height % 2 != 0) {
            throw new IllegalArgumentException("Board must have a width and height of at least 8, and be an even number.");
        }

        squares = new Square[width][height];

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                squares[y][x] = new Square(x, y);
            }
        }
    }

    public void setUpLoadouts(Loadout player1loadout, Loadout player2loadout) {
        // Start by setting loadout 1 pieces in center of board, 2 bottom rows
        // Calculate center of board
        int center = width / 2; // 4
        int loadoutStartCol = center - 4; // 0
        int[] loadout1Rows = {1, 0};
        int[] loadout2Rows = {height - 2, height - 1}; // 6, 7

        // Set loadout 1 pieces
        for (int i = 0; i < player1loadout.getPieces().length; i++) {
            for (int j = 0; j < player1loadout.getPieces()[i].length; j++) {
                Piece piece = player1loadout.getPieces()[i][j];
                Square square = getSquare(loadoutStartCol + j, loadout1Rows[i]);
                square.setPiece(piece);
                piece.setSquare(square);
            }
        }

        // Set loadout 2 pieces
        for (int i = 0; i < player2loadout.getPieces().length; i++) {
            for (int j = 0; j < player2loadout.getPieces()[i].length; j++) {
                Piece piece = player2loadout.getPieces()[i][j];
                Square square = getSquare(loadoutStartCol + j, loadout2Rows[i]);
                square.setPiece(piece);
                piece.setSquare(square);
            }
        }

    }

    public Board(Board that) {
        this.width = that.width;
        this.height = that.height;
        this.squares = new Square[width][height];

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                this.squares[y][x] = new Square(that.squares[y][x]);
            }
        }
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public Square getSquare(int x, int y) {
        return squares[y][x];
    }

    public Square[][] getSquares() {
        return squares;
    }

    public Boolean movePiece(Position from, Position to) {
        Square fromSquare = getSquare(from.getX(), from.getY());
        Square toSquare = getSquare(to.getX(), to.getY());
        Boolean moved = false;

        for (Position validMove : fromSquare.getPiece().getValidMoves()) {
            if (validMove.equals(to)) {
                moved = true;
                Piece movingPiece = fromSquare.getPiece();
                fromSquare.removePiece();
                toSquare.setPiece(movingPiece);
                System.out.println("Piece moved from " + from.getX() + "," + from.getY() + " to " + to.getX() + "," + to.getY());
                Game.getInstance().update();
                return true;
            }
        }
        if (!moved) {
            System.out.println("Invalid move");
            return false;
        }
        return false;
    }

    //Override print method
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Square[] row : squares) {
            for (Square square : row) {
                if (square.isOccupiedByPiece()) {
                    sb.append(square.getPiece().getName().charAt(0));
                    sb.append(" ");
                } else {
                    sb.append("X ");
                }
            }
            sb.append("\n");
        }
        return sb.toString();
    }
}
