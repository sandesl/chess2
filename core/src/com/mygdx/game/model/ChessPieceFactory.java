package com.mygdx.game.model;

import com.mygdx.game.model.Piece.ChessPieceColor;
import com.mygdx.game.model.board.Board;
import com.mygdx.game.model.moves.BishopMoveStrategy;
import com.mygdx.game.model.moves.KingMoveStrategy;
import com.mygdx.game.model.moves.KnightMoveStrategy;
import com.mygdx.game.model.moves.PawnMoveStrategy;
import com.mygdx.game.model.moves.PhoenixMoveStrategy;
import com.mygdx.game.model.moves.QueenMoveStrategy;
import com.mygdx.game.model.moves.RookMoveStrategy;
import com.mygdx.game.model.moves.SnakeMoveStrategy;
import com.mygdx.game.model.moves.ThiefMoveStrategy;
import com.mygdx.game.model.pieces.Bishop;
import com.mygdx.game.model.pieces.King;
import com.mygdx.game.model.pieces.Knight;
import com.mygdx.game.model.pieces.Pawn;
import com.mygdx.game.model.pieces.Phoenix;
import com.mygdx.game.model.pieces.Queen;
import com.mygdx.game.model.pieces.Rook;
import com.mygdx.game.model.pieces.Snake;
import com.mygdx.game.model.pieces.Thief;

public class ChessPieceFactory {

    public static Piece createPiece(String pieceType, ChessPieceColor color, Board board) {
        switch (pieceType) {
            case "BISHOP":
                return new Bishop(color, board, new BishopMoveStrategy());
            case "KING":
                return new King(color, board, new KingMoveStrategy());
            case "KNIGHT":
                return new Knight(color, board, new KnightMoveStrategy());
            case "PAWN":
                return new Pawn(color, board, new PawnMoveStrategy());
            case "QUEEN":
                return new Queen(color, board, new QueenMoveStrategy());
            case "ROOK":
                return new Rook(color, board, new RookMoveStrategy());
            case "THIEF":
                return new Thief(color, board, new ThiefMoveStrategy());
            case "SNAKE":
                return new Snake(color, board, new SnakeMoveStrategy());
            case "PHOENIX":
                return new Phoenix(color, board, new PhoenixMoveStrategy());
            default:
                throw new IllegalArgumentException("Invalid piece type: " + pieceType);
        }
    }

    public static Piece createPiece(Piece piece) {
        String name = piece.getName().toUpperCase();
        ChessPieceColor color = piece.getColor();
        Board board = new Board(piece.getBoard().getWidth(), piece.getBoard().getHeight());
        return createPiece(name, color, board);
    }
}
