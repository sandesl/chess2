package com.mygdx.game.model.player;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.headless.HeadlessFileHandle;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.mygdx.game.model.ChessPieceFactory;
import com.mygdx.game.model.Piece;
import com.mygdx.game.model.board.Board;

import org.json.JSONObject;

import java.util.Arrays;

public class Loadout {
    // Must be a 2x8 array
    // 2 rows, 8 columns
    // First array/row is the front row, row 2
    // Second array/row is the back row, row 1
    // See /assets//loadouts/default.json for an example of the format
    private final Piece[][] pieces;

    public Loadout(Piece[][] pieces) {
        this.pieces = pieces;
    }

    public Loadout(Piece.ChessPieceColor color, Board board) {
        FileHandle handle = Gdx.files.internal("loadouts/default.json");
        String fileContents = handle.readString();
        JSONObject obj = new JSONObject(fileContents);
        pieces = new Piece[2][8];
        JsonValue loadout = new JsonReader().parse(obj.toString()).get("loadout");
        for (int i = 0; i < loadout.size; i++) {
            for (int j = 0; j < loadout.get(i).size; j++) {
                String pieceName = loadout.get(i).get(j).asString();
                Piece piece = ChessPieceFactory.createPiece(pieceName, color, board);
                pieces[i][j] = piece;
            }
        }
    }

    public Loadout(Piece.ChessPieceColor color, String loadoutName, Board board) {
        FileHandle handle = Gdx.files.internal("loadouts/" + loadoutName + ".json");
        String fileContents = handle.readString();
        JSONObject obj = new JSONObject(fileContents);
        pieces = new Piece[2][8];
        JsonValue loadout = new JsonReader().parse(obj.toString()).get("loadout");
        for (int i = 0; i < loadout.size; i++) {
            for (int j = 0; j < loadout.get(i).size; j++) {
                String pieceName = loadout.get(i).get(j).asString();
                Piece piece = ChessPieceFactory.createPiece(pieceName, color, board);
                pieces[i][j] = piece;
            }
        }
    } 

    public Loadout(JSONObject jsobj, Piece.ChessPieceColor color, Board board) {
        // Create an array from the JSON file
        pieces = new Piece[2][8];
        // Get the loadout from the JSON file
        JsonValue loadout = new JsonReader().parse(jsobj.toString()).get("loadout");
        // Iterate through the loadout
        for (int i = 0; i < loadout.size; i++) {
            for (int j = 0; j < loadout.get(i).size; j++) {
                // Get the piece name
                String pieceName = loadout.get(i).get(j).asString();
                // Get the piece from the factory
                Piece piece = ChessPieceFactory.createPiece(pieceName, color, board);
                // Add the piece to the array
                pieces[i][j] = piece;
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Piece[] row : pieces) {
            // Translate each piece to its name
            String[] pieceNames = Arrays.stream(row).map(piece -> piece.getName()).toArray(String[]::new);
            sb.append(Arrays.toString(pieceNames)).append("\n");
        }
        return sb.toString();
    }

    public void printLoadoutWithHeaders() {
        // Print column headers
        System.out.print("  ");
        for (char c = 'a'; c <= 'h'; c++) {
            System.out.printf(" %-10s", c);
        }
        System.out.println();

        // Print each row
        for (int i = 0; i < pieces.length; i++) {
            // Print row number
            System.out.print((i + 1) + " ");

            // Print pieces in the row
            for (int j = 0; j < pieces[i].length; j++) {
                if (pieces[i][j] != null) {
                    System.out.printf(" %-10s", pieces[i][j].getName());
                } else {
                    System.out.printf(" %-10s", " ");
                }
            }
            System.out.println();
        }
    }

    public Piece[][] getPieces() {
        return pieces;
    }

    public static void main(String[] args) {
        // Print current path
        System.out.println("Current path: " + System.getProperty("user.dir"));

        // Initialize headless file handle
        HeadlessFileHandle handle = new HeadlessFileHandle("assets/loadouts/default.json", Files.FileType.Internal);

        //Convert file to string
        String fileContents = handle.readString();

        // Create JSON Object
        JSONObject obj = new JSONObject(fileContents);

        // Print contents
        System.out.println(obj);

        // // Create a new Loadout object
        // Loadout loadout = new Loadout(obj, Piece.ChessPieceColor.WHITE);
        // // Print out the loadout
        // loadout.printLoadoutWithHeaders();
    }
}
