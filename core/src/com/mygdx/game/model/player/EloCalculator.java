package com.mygdx.game.model.player;

import java.util.ArrayList;


// An implementation of https://en.wikipedia.org/wiki/Elo_rating_system
public class EloCalculator {
    // Function to calculate the expected score for player A
    // Returns a float between 0 and 1
    private static float ExpectedScore(float ratingA, float ratingB) {
        return 1.0f / (1 + (float) (Math.pow(10, (ratingA - ratingB) / 400)));
    }

    // Function to calculate Elo rating
    // K is a constant.
    // d determines whether Player A wins
    // or Player B.
    // Return an ArrayList of updated ratings, (aRating, bRating)

    // FIDE K-Factors
    static final int K = 20; // For players under 2400
    static final int K2 = 10; // For players above 2400

    public enum GameEndState {
        WIN, LOSE, DRAW
    }

    // Calculates from perspective of player A
    public static ArrayList<Integer> Calculate2Player(float aRating, float bRating, GameEndState endState) {

        // To calculate the Winning
        // Probability of Player B
        float bExpectedScore = ExpectedScore(aRating, bRating);

        // To calculate the Winning
        // Probability of Player A
        float aExpectedScore = ExpectedScore(bRating, aRating);

        // Case -1 When Player A wins
        // Updating the Elo Ratings
        if (endState == GameEndState.WIN) {
            aRating = aRating + K * (1 - aExpectedScore);
            bRating = bRating + K * (0 - bExpectedScore);
        }

        // Case -2 When Player B wins
        // Updating the Elo Ratings
        else if (endState == GameEndState.LOSE) {
            aRating = aRating + K * (0 - aExpectedScore);
            bRating = bRating + K * (1 - bExpectedScore);
        }

        // Case -3 When Game is Drawn
        // Updating the Elo Ratings
        else {
            aRating = aRating + K * (0.5f - aExpectedScore);
            bRating = bRating + K * (0.5f - bExpectedScore);
        }


        // Return set of updated ratings
        ArrayList<Integer> updatedRatings = new ArrayList<Integer>();
        updatedRatings.add((int) aRating);
        updatedRatings.add((int) bRating);
        return updatedRatings;
    }

    // Test code
    public static void main(String[] args) {

        // Ra and Rb are current ELO ratings
        float Ra = 1200, Rb = 1000;

        int K = 30;
        boolean d = true;

        ArrayList<Integer> newRatings = Calculate2Player(Ra, Rb, GameEndState.WIN);

        // Print updated ratings
        System.out.println("Updated Ratings:");
        System.out.println("Ra = " + newRatings.get(0) + " Rb = " + newRatings.get(1));

    }
}