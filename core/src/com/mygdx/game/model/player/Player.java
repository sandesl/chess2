package com.mygdx.game.model.player;

import com.mygdx.game.model.Piece;

public class Player {
    private final String name;
    private int time;
    private Loadout loadout;
    private Long elo;
    private Piece.ChessPieceColor color;
    public Player(String name) {
        this.name = name;
    }

    public Piece.ChessPieceColor getColor() {
        return this.color;
    }

    public void setColor(Piece.ChessPieceColor color) {
        this.color = color;
    }

    public String getName() {
        return this.name;
    }

    public Long getElo(){return this.elo;}

    public void setElo(Long newElo){ this.elo = newElo; }


    public boolean decreaseTime(){
        this.time-=1;
        return this.time > 0;
    }

    public void setTime(int time){
        this.time = time;
    }

    public String getRemainingTimeString(){
        return String.valueOf(this.time);
    }



}
