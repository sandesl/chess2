package com.mygdx.game.model;

public enum GameStatus {
    ACTIVE,
    CHECKMATE,
    STALEMATE
}
