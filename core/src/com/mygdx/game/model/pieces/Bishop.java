package com.mygdx.game.model.pieces;

import com.mygdx.game.model.Piece;
import com.mygdx.game.model.Position;
import com.mygdx.game.model.board.Board;
import com.mygdx.game.model.moves.IMoveStrategy;
import java.util.List;

public class Bishop extends Piece {

    @Override
    public String getName() {
        return "Bishop";
    }

    public Bishop(ChessPieceColor color, Board board, IMoveStrategy moveStrategy) {
        super(color, board, moveStrategy); // Pass the board to the superclass constructor
    }


}



