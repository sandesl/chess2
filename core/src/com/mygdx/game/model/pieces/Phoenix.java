package com.mygdx.game.model.pieces;

import com.mygdx.game.model.Piece;
import com.mygdx.game.model.board.Board;
import com.mygdx.game.model.moves.IMoveStrategy;

public class Phoenix extends Piece {

    @Override
    public String getName() {
        return "Phoenix";
    }

    public Phoenix(Piece.ChessPieceColor color, Board board, IMoveStrategy moveStrategy) {
        super(color, board, moveStrategy);
    }
}
