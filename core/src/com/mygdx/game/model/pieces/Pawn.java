package com.mygdx.game.model.pieces;

import com.mygdx.game.model.Piece;
import com.mygdx.game.model.Position;
import com.mygdx.game.model.board.Board;
import com.mygdx.game.model.moves.IMoveStrategy;
import java.util.List;

public class Pawn extends Piece {
    @Override
    public String getName() {
        return "Pawn";
    }

    public Pawn(ChessPieceColor color, Board board, IMoveStrategy moveStrategy) {
        super(color, board, moveStrategy);
    }

}
