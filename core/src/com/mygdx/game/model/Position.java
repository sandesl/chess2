package com.mygdx.game.model;

// Starts from bottom left corner
public class Position {
    private final int x;
    private final int y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean equals(Position that) {
        return this.x == that.x && this.y == that.y;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Position position = (Position) obj;
        return x == position.x && y == position.y;
    }

    // To string to generate chess notation position as string, e.g. "a1", allows for infinite sizing, so x,y can be any number
    @Override
    public String toString() {
        char[] alphabet = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        int alphabetLength = alphabet.length;
        int x = this.x;
        int y = this.y;
        // If x is greater than 26, then we need to add another letter, support infinite board size, eg "aaaaaa1"
        int lettersNeeded = x / alphabetLength;
        String position = "";
        for (int i = 0; i < lettersNeeded; i++) {
            position += alphabet[0];
        }
        position += alphabet[x % alphabetLength];
        position += y + 1;
        return position;
    }
}
