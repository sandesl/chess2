package com.mygdx.game.model;

import com.mygdx.game.controller.SoundManager;
import com.mygdx.game.model.board.Board;
import com.mygdx.game.model.player.Loadout;
import com.mygdx.game.model.board.Move;
import com.mygdx.game.model.board.Square;
import com.mygdx.game.model.pieces.King;
import com.mygdx.game.model.player.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class Game {
    private static Game instance;
    private Player currentPlayer;
    private List<Player> players;
    private int mapSize;
    private int time;
    private GameStatus gameStatus;
    private final ArrayList<Move> moves = new ArrayList<>();
    private int currentPlayerIndex; // Start with the first player
    private Board board;
    private final List<King> kings = new ArrayList<>();
    private Player winner;
    private Player loser;
    private Timer timer;

    private Game() {
    }

    public static Game getInstance() {
        if (instance == null) {
            instance = new Game();
        }
        return instance;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public void startGame(List<Player> players, int Mapsize, int time, List<String> loadouts){
        this.players = players;
        setPlayerColors(this.players);
        currentPlayerIndex = 0;
        this.time = time;
        setPlayerTimes(this.players);


        // Game settings
        this.currentPlayer = this.players.get(currentPlayerIndex);
        this.mapSize = Mapsize;
        this.gameStatus = GameStatus.ACTIVE;

        // Initialize the board with default loadouts
        if (this.players.size() == 2) {
            this.board = new Board(mapSize, mapSize);
            this.board.setUpLoadouts(new Loadout(Piece.ChessPieceColor.BLACK,loadouts.get(0),  board), new Loadout(Piece.ChessPieceColor.WHITE, loadouts.get(1), board));
        }

        startTimer(0, 1000);

    }

    public void endGame(){
        instance = null;
    }

    public void switchPlayer() {
        currentPlayerIndex++;
        if (currentPlayerIndex >= players.size()) {
            currentPlayerIndex = 0;
        }
        currentPlayer = players.get(currentPlayerIndex);
    }


    public void togglePause(boolean on){
        if(on){
            this.timer.cancel();
        }else{
            this.startTimer(0, 1000);
        }
    }


    private void setPlayerColors(List<Player> players) {
        if (players.size() == 2) {
            players.get(0).setColor(Piece.ChessPieceColor.WHITE);
            players.get(1).setColor(Piece.ChessPieceColor.BLACK);
        } else if (players.size() == 4) {
            // 4 player logic
            players.get(0).setColor(Piece.ChessPieceColor.WHITE);
            players.get(1).setColor(Piece.ChessPieceColor.BLACK);
        }
    }

    private void updateGameStatus(GameStatus gameStatus) {
        this.gameStatus = gameStatus;
    }

    public List<King> getKings() {
        return this.kings;
    }

    public GameStatus getGameStatus() {
        return this.gameStatus;
    }

    public void update() {
        updateGameStatus(GameStatus.ACTIVE);
        switchPlayer();
    }

    public void movePiece(Piece piece, Square toSquare) {
        Piece.ChessPieceColor pieceColor = piece.getColor();
        Move move;
        Position fromPosition = piece.getPosition();

        // check if game is over
        if (this.gameStatus == GameStatus.CHECKMATE) {
            return;
        }

        // do movement logic here
        if (pieceColor == currentPlayer.getColor()) {
            // Check for capture
            if (toSquare.getPiece() == null) {
                Boolean moved = board.movePiece(piece.getPosition(), toSquare.getPosition());
                if (moved) {
                    move = new Move(fromPosition, toSquare.getPosition(), piece, null, new Board(board));
                    moves.add(move);
                    SoundManager.getInstance().playSound("moveSound");
                }

            }
            else if (pieceColor != toSquare.getPiece().getColor()) {
                if (toSquare.getPiece() instanceof King) {
                    setCheckmate();
                }
                else {
                    Boolean moved = board.movePiece(piece.getPosition(), toSquare.getPosition());
                    if (moved) {
                        move = new Move(fromPosition, toSquare.getPosition(),piece, toSquare.getPiece(), new Board(board));
                        moves.add(move);
                        SoundManager.getInstance().playSound("captureSound");
                    }
                }
            }
        }
    }

    public void setCheckmate() {
        winner = players.get(currentPlayerIndex);
        switchPlayer();
        loser = currentPlayer;
        updateGameStatus(GameStatus.CHECKMATE);
    }

    public Player getWinner() {
        return winner;
    }

    public Player getLoser() {
        return loser;
    }

    public Boolean isColorsTurn(Piece.ChessPieceColor color) {
        return color == currentPlayer.getColor();
    }

    public Board getBoard() {
        return board;
    }

    public void addKing(King king) {
        kings.add(king);
    }

    public Boolean isCheckMate(King king) {
        List<Position> moves = king.getValidMoves();
        List<Position> nonCheckedMoves = getNonCheckedMoves(king, moves);
        return nonCheckedMoves.isEmpty();
    }

    public List<Position> getNonCheckedMoves(Piece king, List<Position> moves) {
        if (!kings.contains(king)) {
            return null;
        }

        List<Position> nonCheckedMoves = new ArrayList<>();
        for (Position move : moves) {
            if (!isKingInCheckOnMove(king, move)) {
                nonCheckedMoves.add(move);
            }
        }
        return nonCheckedMoves;
    }

    public Boolean isKingInCheckOnMove(Piece king, Position position) {
        if (!kings.contains(king)) {
            return null;
        }
        Square squareTo = board.getSquare(position.getX(), position.getY());
        Piece squareToPiece = squareTo.getPiece();
        Square kingSquare = king.getSquare();
        kingSquare.removePiece();
        squareTo.setPiece(king);
        Boolean result = isKingInCheck(king);
        squareTo.removePiece();
        if (squareToPiece != null) {
            squareTo.setPiece(squareToPiece);
        }
        kingSquare.setPiece(king);
        return result;
    }

    private void startTimer(int delay, int period) {
        timer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {


                if(!currentPlayer.decreaseTime()){
                    System.out.println("Ended with time: " + currentPlayer.getRemainingTimeString());
                    switchPlayer();
                    setCheckmate();
                }
            }
        };
        timer.schedule(task, delay, period);
    }



    private void setPlayerTimes(List<Player> players){
        for (Player player: players){
            player.setTime(this.time);

        }
    }





    public Boolean isKingInCheck(Piece king) {
        if (!kings.contains(king)) {
            return null;
        }
        // loop through whole board and get pieces
        for (Square[] row : board.getSquares()) {
            for (Square square : row) {
                if (square.getPiece() == null) {
                    continue;
                }
                if (square.getPiece() instanceof King) {
                    continue;
                }
                if (square.getPiece().getColor() != king.getColor() && attacksKing(square.getPiece().getValidMoves(), king) ) {
                    return true;
                }

            }
        }
        return false;
    }

    public Boolean attacksKing(List<Position> moves, Piece king) {
        for (Position move : moves) {
            if (move.equals(king.getPosition())) {
                return true;
            }
        }
        return false;
    }

    public ArrayList<Move> getMoves() {
        return moves;
    }
}
