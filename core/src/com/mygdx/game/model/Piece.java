package com.mygdx.game.model;

import com.mygdx.game.controller.InGameController;
import com.mygdx.game.model.board.Board;
import com.mygdx.game.model.board.Square;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.mygdx.game.model.moves.IMoveStrategy;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JTextArea;

public abstract class Piece {
    public Board getBoard() {
        return board;
    }

    public enum ChessPieceColor {
        WHITE, BLACK
    }

    @Override
    public String toString() {
        return "Piece{" +
                "name=" + getName() +
                ", color=" + color +
                ", image=" + image +
                '}';
    }

    private final ChessPieceColor color;
    private Square square;

    private Image image;
    private List<Position> validMoves;
    private final Board board;

    public abstract String getName();

    public Piece(ChessPieceColor color, Board board, IMoveStrategy moveStrategy) {
        this.color = color;
        this.board = board;
        this.moveStrategy = moveStrategy;
    }

    public ChessPieceColor getColor() {
        return color;
    }

    public Position getPosition() {
        return square.getPosition();
    }

    public void setSquare(Square square) {
        this.square = square;
    }

    public Square getSquare() {
        return square;
    }
    private final IMoveStrategy moveStrategy;

    public List<Position> getValidMoves() {
        if (moveStrategy == null) {
            throw new RuntimeException("Move strategy not set for piece " + getName());
        }
        return moveStrategy.calculateValidMoves(this);
    }

    public Image getImage() {
        // Done to enable testing of Loadouts without creating pieces with a image file, because it requires libgdx game to be running
        if (image != null) {
            return image;
        }
        String colorDir = "white/";
        if (color == ChessPieceColor.WHITE) {
            colorDir = "white/";
        } else if (color == ChessPieceColor.BLACK) {
            colorDir = "black/";
        }
        image = new Image(new Texture("pieces/" + colorDir + getName().toLowerCase() + ".png"));
        return image;
    }

    public List<Position> getRepeatingMoves(int[][] offsets) {
        final int boardSize = this.board.getHeight();
        this.validMoves = new ArrayList<>();

        for (int[] offset : offsets) {
            // need to make sure it doesnt add its starting position to candidate moves
            int x = this.getPosition().getX() + offset[0];
            int y = this.getPosition().getY() + offset[1];

            while (x >= 0 && x < boardSize && y >= 0 && y < boardSize) {
                Position position = new Position(x, y);
                if (isTargetSameColor(position)) {
                    break;
                }
                this.validMoves.add(new Position(x, y));

                if (Game.getInstance().getBoard().getSquare(x, y).getPiece() != null) {
                    break;
                }
                x += offset[0];
                y += offset[1];
            }
        }
        return this.validMoves;
    }

    public List<Position> getNonReapetingMoves(int[][] offsets) {
        final int boardSize = Game.getInstance().getBoard().getHeight();
        this.validMoves = new ArrayList<>();

        for (int[] offset : offsets) {
            // need to make sure it doesnt add its starting position to candidate moves
            int x = this.getPosition().getX() + offset[0];
            int y = this.getPosition().getY() + offset[1];

            if (x >= 0 && x < boardSize && y >= 0 && y < boardSize) {
                Position position = new Position(x, y);
                if (!isTargetSameColor(position)) {
                    this.validMoves.add(new Position(x, y));
                }

            }
        }

        return validMoves;
    }

    protected boolean isTargetSameColor(Position targetPosition) {
        final Board board = InGameController.getBoard();
        final int targetX = targetPosition.getX();
        final int targetY = targetPosition.getY();

        if (board.getSquare(targetX, targetY) != null) {
            if (board.getSquare(targetX, targetY).getPiece() != null) {
                return board.getSquare(targetX, targetY).getPiece().getColor() == this.getColor();
            }
        }
        return false;
    }
}
