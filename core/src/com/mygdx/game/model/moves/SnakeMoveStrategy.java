package com.mygdx.game.model.moves;

import com.mygdx.game.model.Piece;
import com.mygdx.game.model.Position;
import java.util.List;

public class SnakeMoveStrategy implements IMoveStrategy {

    @Override
    public List<Position> calculateValidMoves(Piece piece) {
        // snake is like a jumping bishop, it moves 2 squares diagonally
        final int[][] offsets = {{2, 2}, {2, -2}, {-2, 2}, {-2, -2}};
        return piece.getRepeatingMoves(offsets);
    }
}
