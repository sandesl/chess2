package com.mygdx.game.model.moves;

import com.mygdx.game.model.Game;
import com.mygdx.game.model.Piece;
import com.mygdx.game.model.Piece.ChessPieceColor;
import com.mygdx.game.model.Position;
import com.mygdx.game.model.board.Board;
import com.mygdx.game.model.board.Square;

import java.util.ArrayList;
import java.util.List;

public class PawnMoveStrategy implements IMoveStrategy {

    @Override
    public List<Position> calculateValidMoves(Piece piece) {
        List<Position> candidateMoves = new ArrayList<>();
        final int boardSize = Game.getInstance().getBoard().getHeight();

        int x = piece.getPosition().getX();
        int y = piece.getPosition().getY();

        // we also need logic for upgrading the pawn when it reaches the end either in p
        // wn class or another class
        if (piece.getColor() == ChessPieceColor.BLACK) {
        // 
            if (y == 1 && (y + 2) < boardSize) {
                candidateMoves.add(new Position(x, y + 2));
            }
            if ((y + 1) < boardSize) {
                candidateMoves.add(new Position(x, y + 1));
            }
        } else {
            if (y == boardSize - 2 && (y - 2) >= 0) {
                candidateMoves.add(new Position(x, y - 2));
            }
            if ((y - 1) >= 0) {
                candidateMoves.add(new Position(x, y - 1));
            }

        }

        // remove possibility of move if there is a piece on the square
        candidateMoves.removeIf(position -> Game.getInstance().getBoard().getSquare(position.getX(), position.getY()).getPiece() != null);


        Board board = Game.getInstance().getBoard();
                
        // now calculate attacks sideways
        if (piece.getColor() == ChessPieceColor.BLACK) {
            if ((x + 1) < boardSize && (y + 1) < boardSize) {
                Square square = board.getSquare(x + 1, y + 1);
                if (square.getPiece() != null && square.getPiece().getColor() != piece.getColor()){
                    candidateMoves.add(new Position(x + 1, y + 1));
                }
            }
            if ((x - 1) >= 0 && (y + 1) < boardSize) {
                Square square = board.getSquare(x - 1, y + 1);
                if (square.getPiece() != null && square.getPiece().getColor() != piece.getColor()) {
                    candidateMoves.add(new Position(x - 1, y + 1));
                }
            }
        }
        // Else, check WHITE
        else {
            if ((x + 1) < boardSize && (y - 1) >= 0) {
                Square square = board.getSquare(x + 1, y - 1);
                if (square.getPiece() != null && square.getPiece().getColor() != piece.getColor()) {
                    candidateMoves.add(new Position(x + 1, y - 1));
                }
            }
            if ((x - 1) >= 0 && (y - 1) >= 0) {
                Square square = board.getSquare(x - 1, y - 1);
                if (square.getPiece() != null && square.getPiece().getColor() != piece.getColor()) {
                    candidateMoves.add(new Position(x - 1, y - 1));
                }
            }
        }

        return candidateMoves;
    }
}
