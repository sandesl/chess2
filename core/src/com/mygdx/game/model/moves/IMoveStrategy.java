package com.mygdx.game.model.moves;

import com.mygdx.game.model.Piece;
import com.mygdx.game.model.Position;

import java.util.List;

public interface IMoveStrategy {
    List<Position> calculateValidMoves(Piece piece);
}
