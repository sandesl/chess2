package com.mygdx.game.model.moves;

import com.mygdx.game.model.Piece;
import com.mygdx.game.model.Position;
import java.util.List;

public class ThiefMoveStrategy implements IMoveStrategy {

    @Override
    public List<Position> calculateValidMoves(Piece piece) {
        // offsets are move calculations, thief can move like a rook with 2 spaces between
        final int[][] offsets = {{2, 0}, {0, 2}, {-2, 0}, {0, -2}};
        return piece.getRepeatingMoves(offsets);

    }
}
