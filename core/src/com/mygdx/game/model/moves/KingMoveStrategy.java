package com.mygdx.game.model.moves;

import com.mygdx.game.model.Game;
import com.mygdx.game.model.Piece;
import com.mygdx.game.model.Position;
import java.util.List;

public class KingMoveStrategy implements IMoveStrategy {

    @Override
    public List<Position> calculateValidMoves(Piece piece) {
        // offsets are move calculations. The king can move one space in any direction
        final int[][] offsets = { { 1, 1 }, { 1, -1 }, { -1, 1 }, { -1, -1 }, { 1, 0 }, { -1, 0 }, { 0, 1 },
                { 0, -1 } };

        List<Position> validMoves = piece.getNonReapetingMoves(offsets);

        return Game.getInstance().getNonCheckedMoves(piece, validMoves);
    }
}
