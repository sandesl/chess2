package com.mygdx.game.model.moves;

import com.mygdx.game.model.Piece;
import com.mygdx.game.model.Position;
import java.util.List;

public class RookMoveStrategy implements IMoveStrategy {

    @Override
    public List<Position> calculateValidMoves(Piece piece) {
        // offsets are move calculations, The rook can move any number of spaces up down
        // left or right
        final int[][] offsets = { { 1, 0 }, { -1, 0 }, { 0, 1 }, { 0, -1 } };

        return piece.getRepeatingMoves(offsets);
    }
}
