package com.mygdx.game.model.moves;

import com.mygdx.game.model.Piece;
import com.mygdx.game.model.Position;
import java.util.List;

public class QueenMoveStrategy implements IMoveStrategy {

    @Override
    public List<Position> calculateValidMoves(Piece piece) {
        // offsets are move calculations, The queen can move in any direction any number
        // of spaces
        final int[][] offsets = { { 1, 1 }, { 1, -1 }, { -1, 1 }, { -1, -1 }, { 1, 0 }, { -1, 0 }, { 0, 1 },
                { 0, -1 } };

        return piece.getRepeatingMoves(offsets);

    }
}
