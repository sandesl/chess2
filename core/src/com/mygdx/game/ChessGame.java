package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.utils.ScreenUtils;
import com.mygdx.game.controller.SoundManager;
import com.mygdx.game.service.APIClient.APIClientProvider;

public class ChessGame extends ApplicationAdapter {
    public static final float WIDTH = 540;
    public static final float HEIGHT = 960;
    private final int fps = 60;

    APIClientProvider apiProvider;

    public ChessGame() {
        super();
    }
    FPSLogger logger = new FPSLogger();

    public ChessGame(APIClientProvider apiProvider) {
        super();
        this.apiProvider = apiProvider;
    }

    @Override
    public void create() {
        GameManager.getInstance().setApiClientProvider(apiProvider);
        SoundManager.getInstance().playMusic("gameMusic");
    }

    @Override
    public void render() {
        ScreenUtils.clear(0.3f, 0, 0, 1);
        sleep(fps);
        logger.log(); // Log fps
        GameManager.getInstance().render();
    }

    @Override
    public void dispose() {
        GameManager.getInstance().dispose();
    }

    @Override
    public void resize(int width, int height) {
        GameManager.getInstance().resize(width, height);
    }


    private long diff, start = System.currentTimeMillis();

    public void sleep(int fps) {
        if(fps>0){
            diff = System.currentTimeMillis() - start;
            long targetDelay = 1000/fps;
            if (diff < targetDelay) {
                try{
                    Thread.sleep(targetDelay - diff);
                } catch (InterruptedException e) {}
            }
            start = System.currentTimeMillis();
        }
    }

}
