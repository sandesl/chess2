# Chess Evolution

This project is a new chess game implemented in Java using the LibGDX framework. The game is designed to be played on an android device.

## How to run

To run the game, first make sure you have an emulator or a physical device connected and running.
Then you can run the game by running the game from the Android module.

## Structure

The game is created with LibGDX, and is divided into five main modules/packages: 'android', 'core', 'tests', 'puml', 'assets'.

The 'android' and 'core' modules holds the game logic and functionality. The 'android' module holds everything necessary for running the game as an Android game.
The 'core' module includes all the common logic and functionality needed regardless of which platform the game is intended to be played on.

The 'tests' module includes general tests for the game.

The 'puml' module includes all the PUML diagrams about the project's structure.

The 'assets' module includes all the assets.